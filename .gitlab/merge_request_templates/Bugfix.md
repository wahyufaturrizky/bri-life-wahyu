## Issue Identifier

Identifier of issue. Example "#6"

## Relevant Issue (optional)

If this PR solve or referenced to another issue, please list those issue numbers in here.

Example "#32, #45"

## Screenshot Results

Screenshot that verify this PR solves the issue. Example screenshot of App screen, website, or console.

## Screenshot of Test Report (optional)

screenshot displaying your test report from jasmine reporter or any test framework this project use.

Example
```bash
Cart
*  should add carrot in the cart list [success]
*  ...
```

## How do you solve this issue ?

Explain briefly and elaborate in sequential step in order to solve this issue if you cannot explain it in briefly manner,

example
max-height in cart is 300px, you should make it responsive

1. edit the `main.css`
2. ...
