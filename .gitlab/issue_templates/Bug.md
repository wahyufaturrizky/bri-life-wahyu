## Descriptions
Describe the problems (max 500 words).

Example "Items not displayed if there is more than 12 item in cart"

## Reproduction Steps
Describe in specific sequence step, one must take in order for the bug to happen

Example

1. Login to app
2. Add 12 product item into cart
3. Click cart menu on the header-bar
4. Scroll to bottom of item list
5. Add item "Carrot"
6. Click cart menu on the header-bar
7. Scroll to bottom of item list

## Expected behavior
What should happen

Example "Item Carrot should be in the bottom of cart"

## Observed behavior
What actually happen

Example "Item Carrot not in the cart"

## Severity (Optional)

Your initial impression of this issue, how severe is that.

Use these rates as your impression

- `Blocker` This is a issue so severe that the system cannot reasonably be released without either fixing it or devising a workaround
- `Critical` Although the system could still be released with a issue of this magnitude, it severely impacts the core functionality of the program or makes it almost unusable
- `Major` This is a issue which causes a relatively severe problem, although not so severe as to entirely hobble a system.
- `Normal` This is a issue which inconveniences the user, or a more severe issue with an easy and straightforward workaround.
- `Minor` The issue might or might not be noticed, but does not cause much issue for the user, or has a simple and straightforward workaround.
- `Trivial` The issue would probably not even be noticed
- `Enhancement` it’s something that the users want, but is not specified by the requirements or is otherwise not in the scope of this project.

p.s. please use your best judgement

## Workaround (optional)

how the issue can be avoided

Example

"Buy another item after checkout"

## Environment

Specify every environment detail that might help developer to reproduce the bug.

Example
- Device : Iphone 5C
- IOS Version : 10.3
- App Version: 1.3

## Notes

Everything that will help / useful for solve / track this issue. Like `stack-traces`, `screenshot`, `configs`, `the app you installed`, `OS` etc..

Example
- attach a screenshot
