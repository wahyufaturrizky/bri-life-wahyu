Contributing
===========================================

Development Workflow
--------------------

Lussa follows [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) for collaboration guide.

### New Feature

If you are developer, follow this guideline before coding new feature.

1. clone the project, use your username & password from lussa git account

    ``` bash
    git clone https://git.lussa.net/admedika/mypersona-mobile
    ```
2. make new feature branch from `develop` branch

    ``` bash
    git checkout -b develop origin/develop
    git checkout -b feature/name develop
    
    //example
    git checkout -b feature/chat
    ```

    use feature name that can describe higlights of feature you want to develop, like `authentication-module` or `chat`. 

3. code, stage & commit your feature to feature branch each time you finish some checkpoint. dont forget to push it.

    ``` bash
    git status
    git add -all
    git commit -m "commit message"
    git push -u origin feature/name
    ```

    Commit message best practices
    - Use minimum **50 character** for headline of commit message
    - dont commit **commented code** you can revert or lookup your history next time.
    - You can add detail commit message on new line
    - use imperative word like `add`, `fix`, `improve`
    - commit minimum 1 commit / day to keep your code fresh

4. Your feature is finished ? time to create merge request
    - go to [merge request page](https://git.lussa.net/admedika/mypersona-mobile/merge_requests)
    - create new merge request from your `feature branch` as source branch to `develop branch` as target branch
    - pick `feature submission` as template
    - use title with this format *[feature] <highlights-of-your-feature>*
    - wait for test & reply for finalization of your feature
    - always keep your feature up-to-date with `develop` branch

_You can develop other feature while waiting your merge request is reviewed by QA_


### Bug Fix

If you are developer, follow this guideline before coding a bug fix.

1. clone the project, use your username & password from lussa git account

    ``` bash
    git clone https://git.lussa.net/admedika/mypersona-mobile
    ```
2. make new hotfix branch from `master` branch

    ``` bash
    git checkout -b master origin/master
    git checkout -b hotfix/[version] develop
    
    //example
    git checkout -b hotfix/1.0.1
    ```

    check the version from [tag pages](https://git.lussa.net/admedika/mypersona-mobile/tags) or via `git tag` command. A hotfix should increase the version by 0.0.1. For ex: if the latest tag is 1.2.5, the current hotfix tag should be 1.2.6.

3. code, stage & commit your feature to feature branch each time you finish some checkpoint. dont forget to push it.

    ``` bash
    git status
    git add -all
    git commit -m "commit message"
    git push -u origin hotfix/version
    ```

    Commit message best practices
    - Use minimum **50 character** for headline of commit message
    - dont commit **commented code** you can revert or lookup your history next time.
    - You can add detail commit message on new line
    - use imperative word like `add`, `fix`, `improve`
    - commit minimum 1 commit / day to keep your code fresh

4. Your feature finished ? time to create merge request
    - go to [merge request page](https://git.lussa.net/admedika/mypersona-mobile/merge_requests)
    - create new merge request from your `hotfix branch` as source branch to `master branch` as target branch
    - pick `bugfix submission` as template
    - use title with this format *[bugfix] #issue-number*
    - wait for test & reply for finalization of your bugfix