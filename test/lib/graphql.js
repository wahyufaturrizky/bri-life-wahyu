var request = require('request-promise');

module.exports = function graphql(query, variables, token) {
  return request({
    method: 'POST',
    uri: 'http://139.59.98.26:4321/graphql',
    headers: { Authorization: 'JWT ' + token },
    body: { query: query, variables: variables },
    json: true
  });
};
