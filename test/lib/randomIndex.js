module.exports = function randomIndex(max) {
  return Math.floor((Math.random() * max)) || 0;
};
