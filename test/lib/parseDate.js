module.exports = function parseDate(date) {
  return date.replace(/-/g, '/');
};
