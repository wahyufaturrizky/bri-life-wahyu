module.exports = function parseFrequencyDesc(desc) {
  return 'per '.concat(desc.replace(/per\s+/gi, ''));
};
