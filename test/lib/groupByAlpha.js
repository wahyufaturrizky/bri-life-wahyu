var _ = require('underscore');

function getProviderName(value, nearMe) {
  var name = value && value.replace(/\s\s+/g, ' ').split(' ');

  if (nearMe) {
    return 'Nearest...';
  }

  if (name && name.length > 1) {
    if (_.contains(['&', '-'], name[1]) && name[2] && name[3]) return name[3].toLowerCase();
    return name[1].toLowerCase();
  }

  return value && value.toLowerCase();
}

module.exports = function groupByAlpha(data, nearMe) {
  return _.groupBy(data, function each(val) {
    var name = getProviderName(val.providerName.replace(/[^a-z& ]/gi, '').trim(), nearMe);

    if (nearMe) return name;
    return name && _.first(name.toUpperCase());
  });
};
