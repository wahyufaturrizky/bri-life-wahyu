var moment = require('moment');

module.exports = function parseBirthdate(date) {
  if (date && moment(new Date(date)).isValid()) {
    return moment(new Date(date)).format('DD MMM YYYY');
  }
  return date;
};
