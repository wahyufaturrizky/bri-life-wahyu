var moment = require('moment');

module.exports = function parseClaimdate(date) {
  if (date && moment(new Date(date)).isValid()) {
    return moment(new Date(date)).format('MMM/DD/YYYY');
  }
  return '-';
};
