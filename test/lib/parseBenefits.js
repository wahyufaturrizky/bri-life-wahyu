module.exports = function parseBenefits(benefits) {
  return benefits && benefits.map(function onEach(benefit) {
    return Object.prototype.hasOwnProperty.call(benefit, 'planType') && benefit.planType;
  }).filter(Boolean).join(', ');
};
