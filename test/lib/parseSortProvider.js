module.exports = function parseSortProvider(value) {
  var name = value && value.replace(/\s\s+/g, ' ').split(' ');

  if (name && name.length > 1) {
    if ((name[1] === '&' || name[1] === '-') && name[2] && name[3]) return name[3].toLowerCase();
    return name[1].toLowerCase();
  }

  return value && value.toLowerCase();
};
