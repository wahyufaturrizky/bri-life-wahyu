module.exports = function reverse(text) {
  return String(text).split('').reverse().join('');
};
