module.exports = function parseHtml(text) {
  return '<div ng-bind-html="trustedHtml" class="ng-binding">' +
            text.replace(/\/>/gi, '>').replace(/&#34;/gi, '"') +
         '</div>';
};
