module.exports = function titleize(text) {
  return text && text.toLowerCase().replace(/\s\s+/g, ' ').replace(/(?:^|\s|-)\S/g, function each(word) {
    return word.toUpperCase();
  });
};
