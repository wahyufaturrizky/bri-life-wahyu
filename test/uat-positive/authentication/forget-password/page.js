module.exports = {
  button: {
    submit: by.css('button[type="submit"]')
  },
  input: {
    email: by.name('email')
  },
  popup: {
    container: by.css('.popup-container.popup-showing.active'),
    close: by.buttonText('Close')
  },
  view: by.css('ion-view[nav-view="active"][view-title="Forget Password"]'),
  route: 'auth/forget-password'
};
