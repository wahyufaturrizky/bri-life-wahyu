var forgetPasswordSuccess = require('../forget-password-success/page');
var login = require('../login/page');
var register = require('../register/data');

var page = require('./page');

describe(':: Forget Password ::', function todo() {
  it('should go to forget password page', function be() {
    // click forget password
    browser.wait(element(login.button.forget).isPresent(), 5000, 'wait for forget password');
    browser.actions().mouseMove(element(login.button.forget)).click().perform();
    // open forget password page
    expect(browser.getCurrentUrl()).toContain(page.route);
  });

  it('should success reset password', function be() {
    // input field email
    element(page.view).element(page.input.email).clear().sendKeys(register.email);
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for submit');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // open forget password success page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(forgetPasswordSuccess.route);
  });
});
