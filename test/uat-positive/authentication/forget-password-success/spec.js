var login = require('../login/page');

var page = require('./page');

describe(':: Forget Password Success ::', function todo() {
  it('should go to login page', function be() {
    // click login button
    browser.wait(element(page.view).element(page.button.login).isDisplayed(), 5000, 'wait for login');
    browser.actions().mouseMove(element(page.view).element(page.button.login)).click().perform();
    // open login page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(login.route);
  });
});
