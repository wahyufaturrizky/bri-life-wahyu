module.exports = {
  button: {
    login: by.id('login')
  },
  view: by.css('ion-view[nav-view="active"][view-title="Forget Password Success"]'),
  route: 'auth/forget-password-success'
};
