module.exports = {
  button: {
    forget: by.partialButtonText('FORGET'),
    register: by.partialButtonText('REGISTER'),
    submit: by.css('button[type="submit"]')
  },
  input: {
    username: by.name('username'),
    password: by.name('password')
  },
  popup: {
    container: by.css('.popup-container.popup-showing.active'),
    close: by.buttonText('Tutup'),
    loginError: by.id('login-error'),
    noCredential: by.id('no-credential')
  },
  view: by.css('ion-view[nav-view="active"][view-title="Login"]'),
  route: 'auth/login'
};
