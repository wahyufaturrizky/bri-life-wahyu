var data = require('./data');
var page = require('./page');

exports.doLogin = function doLogin() {
  // input field username
  element(page.view).element(page.input.username).clear().sendKeys(data.email);
  // input field password
  element(page.view).element(page.input.password).clear().sendKeys(data.password);
  // click submit
  browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for login');
  browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
  // open home page
  browser.waitForAngular();
  browser.getTitle().then(function resolve(res) {
    expect(res).toMatch(/home/i);
  });
};

describe(':: Login ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should success login and go to home page', exports.doLogin);
});
