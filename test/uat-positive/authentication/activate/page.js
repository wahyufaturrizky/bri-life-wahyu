module.exports = {
  button: {
    submit: by.css('button[type="submit"]')
  },
  input: {
    code: by.model('data.code')
  },
  popup: {
    container: by.css('.popup-container.popup-showing.active'),
    close: by.buttonText('Close')
  },
  view: by.css('ion-view[nav-view="active"][view-title="Activate Account"]'),
  route: 'auth/activate'
};
