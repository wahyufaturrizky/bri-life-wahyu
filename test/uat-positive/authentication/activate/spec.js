var login = require('../login/page');
var register = require('../register/data');

var data = require('./data');
var page = require('./page');

describe(':: Activation ::', function todo() {
  it('should success activation and go to login page', function be() {
    // get activation code from server
    data.getActivationCode(register.email).then(function resolve(res) {
      if (!(res && res.data && res.data.activationCode)) {
        throw new Error('cannot get activatiion code');
      }
      // input field code
      browser.waitForAngular();
      element(page.view).element(page.input.code).clear().sendKeys(res.data.activationCode);
    });
    // click submit button
    browser.driver.sleep(1234);
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for submit');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // open login page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(login.route);
  });
});
