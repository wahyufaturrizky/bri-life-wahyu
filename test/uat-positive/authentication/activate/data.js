var graphql = require('../../../lib/graphql');

module.exports = {
  getActivationCode: function getActivationCode(email) {
    var variables = { email: email };
    var query = [
      'query ActivationCode($email: String!) {',
      '  activationCode(email: $email)',
      '}'
    ].join('');
    return graphql(query, variables);
  }
};
