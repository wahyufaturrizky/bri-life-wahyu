var random = require('randomstring');

module.exports = {
  name: random.generate({ charset: 'alphabetic', length: 10 }),
  phone: random.generate({ charset: 'numeric', length: 10 }),
  email: 'test-' + String(random.generate({ charset: 'numeric', length: 4 })).concat('@local.dev'),
  password: 'sukses2017',
  cardNumber: '676767676767676'
};
