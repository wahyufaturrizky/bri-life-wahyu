module.exports = {
  agreement: {
    modal: by.id('agreement'),
    agree: by.id('agree'),
    disagree: by.id('disagree')
  },
  button: {
    submit: by.css('button[type="submit"]')
  },
  captcha: by.binding('$.code'),
  datepicker: {
    day: by.css('.date_cell'),
    month: by.repeater('month in monthsList'),
    year: by.options('year for year in yearsList'),
    popup: by.css('.popup-container.ionic_datepicker_popup.popup-showing.active')
  },
  genderpicker: by.repeater('(key, val) in { male: \'Pria\', female: \'Wanita\' }'),
  input: {
    name: by.model('data.name'),
    birthday: by.model('data.birthday'),
    gender: by.model('data.gender'),
    phone: by.model('data.phone'),
    email: by.model('data.email'),
    cardNumber: by.model('data.cardNumber'),
    password: by.model('data.password'),
    passwordConfirmation: by.model('data.passwordConfirmation'),
    code: by.model('data.code')
  },
  popup: {
    container: by.css('.popup-container.popup-showing.active'),
    message: by.binding('data.message'),
    ok: by.buttonText('OK')
  },
  view: by.css('ion-view[nav-view="active"][view-title="Register"]'),
  route: 'auth/register'
};
