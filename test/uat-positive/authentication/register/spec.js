var randomIndex = require('../../../lib/randomIndex');

var activate = require('../activate/page');
var login = require('../login/page');

var data = require('./data');
var page = require('./page');

describe(':: Register ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
  });

  it('should display login page', function be() {
    expect(browser.getCurrentUrl()).toContain(login.route);
  });

  it('should go to register page', function be() {
    // click register button
    browser.wait(element(login.view).element(login.button.register).isPresent(), 5000, 'wait for register');
    browser.actions().mouseMove(element(login.view).element(login.button.register))
      .click().perform();
    // open register page
    expect(browser.getCurrentUrl()).toContain(page.route);
  });

  it('should success register and go to activation page', function be() {
    // input field name
    element(page.view).element(page.input.name).clear().sendKeys(data.name);
    // click field birthday
    browser.actions().mouseMove(element(page.view).element(page.input.birthday)).click().perform();
    // show popup datepicker
    browser.wait(element(page.datepicker.popup).isDisplayed(), 5000, 'wait for datepicker popup');
    // select datepicker month
    browser.wait(element(page.datepicker.month).isPresent(), 5000, 'wait for datepicker month');
    element.all(page.datepicker.month).then(function resolve(options) {
      options[randomIndex((new Date()).getMonth())].click();
    });
    // select datepicker year
    browser.wait(element(page.datepicker.year).isPresent(), 5000, 'wait for datepicker year');
    element.all(page.datepicker.year).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // select datepicker date
    browser.wait(element(page.datepicker.day).isPresent(), 5000, 'wait for datepicker date');
    element.all(page.datepicker.day).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // click field gender
    browser.wait(element(page.view).element(page.input.gender).isDisplayed(), 5000, 'wait for gender field');
    element.all(page.genderpicker).then(function resolve(options) {
      // select gender
      options[randomIndex(options.length)].click();
    });
    // input field phone
    browser.wait(element(page.view).element(page.input.phone).isDisplayed(), 5000, 'wait for phone field');
    element(page.view).element(page.input.phone).clear().sendKeys(data.phone);
    // input field email
    element(page.view).element(page.input.email).clear().sendKeys(data.email);
    // input field password
    element(page.view).element(page.input.password).clear().sendKeys(data.password);
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear().sendKeys(data.password);
    // input field code
    element(page.view).element(page.input.code).clear().sendKeys(element(page.captcha).getText());
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show modal agreement
    browser.waitForAngular();
    browser.wait(element(page.agreement.modal).isDisplayed(), 5000, 'wait for agreement');
    expect(element(page.agreement.disagree).isPresent()).toBeTruthy();
    // click agree
    browser.wait(element(page.agreement.agree).isPresent(), 5000, 'wait for agree');
    browser.actions().mouseMove(element(page.agreement.agree)).click().perform();
    // open activation page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(activate.route);
  });
});
