var doLogin = require('../login/spec').doLogin;
var login = require('../login/page');

var page = require('./page');

describe(':: Logout ::', function todo() {
  it('should logout', function be() {
    // click sidemenu toggle
    browser.wait(element(page.view).element(page.button.sidemenu).isDisplayed(), 5000, 'wait for sidemenu');
    browser.actions().mouseMove(element(page.view).element(page.button.sidemenu)).click().perform();
    // show sidemenu
    expect(element(page.sidebar).isPresent()).toBeTruthy();
    // click logout menu
    browser.wait(element(page.sidemenu.logout).isDisplayed(), 5000, 'wait for logout');
    browser.actions().mouseMove(element(page.sidemenu.logout)).click().perform();
    // open login page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(login.route);
    // login
    doLogin();
  });
});
