module.exports = {
  button: {
    sidemenu: by.id('sidemenu')
  },
  view: by.css('ion-side-menus[nav-view="active"] [nav-bar="active"]'),
  sidebar: by.className('menu-open'),
  sidemenu: {
    logout: by.id('sidemenu-logout')
  }
};
