var parseClaimdate = require('../../../lib/parseClaimdate');
var titleize = require('../../../lib/titleize');

var member = require('../member/page');

var data = require('./data');
var page = require('./page');

function randomIndex(max) {
  return Math.floor((Math.random() * max)) || 0;
}

function validateClaim(claim, index) {
  expect(element(page.view).element(page.claims.row(index).column('claim.claimId')).getText())
    .toEqual(String(claim.claimId));
  expect(element(page.view).element(page.claims.row(index).column('claim.status')).getText())
    .toEqual(claim.status);
  expect(element(page.view).element(page.claims.row(index).column('claim.providerName')).getText())
    .toEqual(claim.providerName);
  expect(element(page.view).element(page.claims.row(index).column('claim.planType')).getText())
    .toEqual(claim.planType || '-');
  expect(element(page.view).element(page.claims.row(index).column('claim.admissionDate')).getText())
    .toEqual(parseClaimdate(claim.admissionDate));
  expect(element(page.view).element(page.claims.row(index).column('claim.diagnosis')).getText())
    .toEqual(claim.diagnosis);
  expect(element(page.view).element(page.claims.row(index).column('claim.dischargeDate')).getText())
    .toEqual(parseClaimdate(claim.dischargeDate));
  expect(element(page.view).element(page.claims.row(index).column('claim.incured')).getText())
    .toEqual('RP. ' + Number.parseFloat(claim.incured).toLocaleString());
  expect(element(page.view).element(page.claims.row(index).column('claim.approved')).getText())
    .toEqual('RP. ' + Number.parseFloat(claim.approved).toLocaleString());
  expect(element(page.view).element(page.claims.row(index).column('claim.excess')).getText())
    .toEqual('RP. ' + Number.parseFloat(claim.excess).toLocaleString());
  expect(element(page.view).element(page.claims.row(index).column('claim.remarks')).getText())
    .toEqual(claim.remarks || '-');
}

describe(':: Claim History ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(member.route);
    // click claim menu
    expect(element(member.view).element(member.menu.claim).isDisplayed()).toBeTruthy();
    browser.actions().mouseMove(element(member.view).element(member.menu.claim))
      .click().perform();
    // open claim page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
  });

  it('should show list of claims', function be() {
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getClaims(res);
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      if (item && item.claims && item.claims.length) {
        item.claims.filter(function each(claim) {
          return claim && claim.claimId;
        }).forEach(function each(claim, index) {
          validateClaim(claim, index);
        });
      } else {
        expect(element(page.view).element(page.claims).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of claims by sort', function be() {
    // click sort
    browser.actions().mouseMove(element(page.view).element(page.button.sort)).click().perform();
    browser.waitForAngular();
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getClaims(res, { sort: 'dischargeDate' });
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      if (item && item.claims && item.claims.length) {
        item.claims.filter(function each(claim) {
          return claim && claim.claimId;
        }).forEach(function each(claim, index) {
          validateClaim(claim, index);
        });
      } else {
        expect(element(page.view).element(page.claims).isPresent()).toBeFalsy();
      }
    });
    // click sort
    browser.actions().mouseMove(element(page.view).element(page.button.sort)).click().perform();
  });

  it('should show list of claims by benefit', function be() {
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getBenefitClaims(res);
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      var value;
      if (item && item.plans && item.plans.length) {
        expect(element(page.view).element(page.benefits).isPresent()).toBeTruthy();
        expect(element(page.view).all(page.benefits).getText())
          .toEqual(item.plans.map(function each(plan) {
            return plan && titleize(plan.planType);
          }).filter(Boolean));
        // select benefit
        element(page.view).all(page.benefits).then(function select(options) {
          var index = randomIndex(options.length);
          options[index].click();
          options[index].getAttribute('value').then(function opts(val) {
            value = val;
          });
        });
        browser.waitForAngular();
        browser.executeScript('return window.localStorage.getItem(\'token\');').then(function success(token) {
          if (!(token)) return null;
          return data.getClaims(token, { planId: value });
        }).then(function success(result) {
          item = result && result.data && result.data.profile && result.data.profile.admedika;
          if (item && item.claims && item.claims.length) {
            item.claims.filter(function each(claim) {
              return claim && claim.claimId;
            }).forEach(function each(claim, index) {
              validateClaim(claim, index);
            });
          } else {
            expect(element(page.view).element(page.claims).isPresent()).toBeFalsy();
          }
        });
        // select benefit
        element(page.view).element(page.input.benefit).click();
      } else {
        expect(element(page.view).element(page.benefits).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of claims by date', function be() {
    var startDate;
    var endDate;
    // click date range
    browser.actions().mouseMove(element(page.view).element(page.button.range)).click().perform();
    // show popup date range
    browser.wait(element(page.popup).isDisplayed(), 5000, 'wait for popup');
    expect(element(page.modal.daterange).isDisplayed()).toBeTruthy();
    // click field start date
    browser.actions().mouseMove(element(page.input.startDate)).click().perform();
    // show popup datepicker
    browser.wait(element(page.datepicker.popup).isDisplayed(), 5000, 'wait for datepicker popup');
    // select datepicker month
    browser.wait(element(page.datepicker.month).isPresent(), 5000, 'wait for datepicker month');
    element.all(page.datepicker.month).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // select datepicker year
    browser.wait(element(page.datepicker.year).isPresent(), 5000, 'wait for datepicker year');
    element.all(page.datepicker.year).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // select datepicker date
    browser.wait(element(page.datepicker.day).isPresent(), 5000, 'wait for datepicker date');
    element.all(page.datepicker.day).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // click field end date
    browser.wait(element(page.input.endDate).isDisplayed(), 5000, 'wait for input');
    browser.actions().mouseMove(element(page.input.endDate)).click().perform();
    // show popup datepicker
    browser.wait(element(page.datepicker.popup).isDisplayed(), 5000, 'wait for datepicker popup');
    // select datepicker month
    browser.wait(element(page.datepicker.month).isPresent(), 5000, 'wait for datepicker month');
    element.all(page.datepicker.month).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // select datepicker year
    browser.wait(element(page.datepicker.year).isPresent(), 5000, 'wait for datepicker year');
    element.all(page.datepicker.year).then(function resolve(options) {
      options[randomIndex(options.length - 2) + 1].click();
    });
    // select datepicker date
    browser.wait(element(page.datepicker.day).isPresent(), 5000, 'wait for datepicker date');
    element.all(page.datepicker.day).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    element(page.input.startDate).getAttribute('value').then(function resolve(res) {
      startDate = res;
    });
    element(page.input.endDate).getAttribute('value').then(function resolve(res) {
      endDate = res;
    });
    // click submit
    browser.wait(element(page.modal.submit).isDisplayed(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.modal.submit)).click().perform();
    browser.waitForAngular();
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getClaims(res, { startDate: startDate, endDate: endDate });
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      if (item && item.claims && item.claims.length) {
        item.claims.filter(function each(claim) {
          return claim && claim.claimId;
        }).forEach(function each(claim, index) {
          validateClaim(claim, index);
        });
      } else {
        expect(element(page.view).element(page.claims).isPresent()).toBeFalsy();
      }
    });
  });
});
