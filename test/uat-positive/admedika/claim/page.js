module.exports = {
  input: {
    benefit: by.id('claim-benefit'),
    startDate: by.model('data.startDate'),
    endDate: by.model('data.endDate')
  },
  button: {
    sort: by.id('claim-sort'),
    range: by.id('claim-range')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Claim History"]'),
  benefits: by.repeater('option in $.profile.admedika.plans'),
  claims: by.repeater('claim in $.claims'),
  popup: by.css('.popup-container.popup-showing.active'),
  datepicker: {
    day: by.css('.date_cell'),
    month: by.repeater('month in monthsList'),
    year: by.options('year for year in yearsList'),
    popup: by.css('.popup-container.ionic_datepicker_popup.popup-showing.active')
  },
  modal: {
    daterange: by.id('date-range'),
    submit: by.buttonText('Submit')
  },
  route: 'admedika/claim'
};
