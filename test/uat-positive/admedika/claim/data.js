var graphql = require('../../../lib/graphql');

module.exports = {
  getBenefitClaims: function getBenefitClaims(token) {
    var query = [
      'query Plans {',
      '  profile {',
      '    admedika {',
      '      plans {',
      '        planId',
      '        planType',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, {}, token);
  },
  getClaims: function getClaims(token, params) {
    var options = params || {};
    var variables = {
      skip: 0,
      limit: 5,
      sort: options.sort || '-dischargeDate',
      planId: options.planId || '*',
      startDate: options.startDate || '',
      endDate: options.endDate || ''
    };
    var query = [
      'query Claim($planId: String, $skip: Int, $limit: Int, $sort: String, $startDate: String, $endDate: String) {',
      '  profile {',
      '    admedika {',
      '      claims(planId: $planId, skip: $skip, limit: $limit, sort: $sort, startDate: $startDate, endDate: $endDate) {',
      '        claimId',
      '        status',
      '        claimType',
      '        admissionDate',
      '        dischargeDate',
      '        planType',
      '        diagnosis',
      '        providerName',
      '        incured',
      '        excess',
      '        approved',
      '        remarks',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  }
};
