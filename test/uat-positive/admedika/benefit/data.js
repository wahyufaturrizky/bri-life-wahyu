var graphql = require('../../../lib/graphql');

module.exports = {
  getBenefits: function getBenefits(token, params) {
    var variables = { planId: params.planId };
    var query = [
      'query Benefit($planId: String) {',
      '  profile {',
      '    admedika {',
      '      benefits(planId: $planId) {',
      '        benefitName',
      '        maxIdr',
      '        frequencyDesc',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  },
  getPlans: function getPlans(token) {
    var variables = { skip: 0, limit: 10, sort: 'planType' };
    var query = [
      'query Plan($skip: Int, $limit: Int, $sort: String) {',
      '  profile {',
      '    admedika {',
      '      plans(skip: $skip, limit: $limit, sort: $sort) {',
      '        planId',
      '        planType',
      '        policyStartDate',
      '        policyEndDate',
      '        recordStatus',
      '        maxIdr',
      '        frequencyDesc',
      '        currentLimit',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  }
};
