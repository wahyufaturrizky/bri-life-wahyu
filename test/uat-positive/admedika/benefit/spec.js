var parseDate = require('../../../lib/parseDate');
var parseFrequencyDesc = require('../../../lib/parseFrequencyDesc');

var member = require('../member/page');

var data = require('./data');
var page = require('./page');

describe(':: Benefit ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(member.route);
  });

  it('should show benefit info', function be() {
    // click benefit menu
    expect(element(member.view).element(member.menu.benefit).isDisplayed()).toBeTruthy();
    browser.actions().mouseMove(element(member.view).element(member.menu.benefit))
      .click().perform();
    // open benefit page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getPlans(res);
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      if (item && item.plans && item.plans.length) {
        item.plans.filter(function each(plan) {
          return plan && plan.planId;
        }).forEach(function each(plan, index) {
          expect(element(page.view).element(page.plans.row(index).column('plan.planType')).getText())
            .toEqual(plan.planType);
          expect(element(page.view).element(page.plans.row(index).column('plan.policyStartDate')).getText())
            .toEqual(parseDate(plan.policyStartDate) + ' - ' + parseDate(plan.policyEndDate));
          expect(element(page.view).element(page.plans.row(index).column('plan.maxIdr')).getText())
            .toEqual(Number.parseFloat(plan.maxIdr).toLocaleString() + ' ' + plan.frequencyDesc);
          // click item
          element(page.view).element(page.plans.row(index)).click();
          browser.waitForAngular();
          //
          browser.executeScript('return window.localStorage.getItem(\'token\');').then(function success(token) {
            if (!(token)) return null;
            return data.getBenefits(token, plan);
          }).then(function success(result) {
            item = result && result.data && result.data.profile && result.data.profile.admedika;
            if (item && item.benefits && item.benefits.length) {
              item.benefits.filter(function every(benefit) {
                return benefit && benefit.benefitName;
              }).forEach(function every(benefit, key) {
                expect(element(page.view).element(page.plans.row(index)).element(page.benefits.row(key).column('item.benefitName')).getText())
                  .toEqual(benefit.benefitName.toUpperCase() + ' ' + parseFrequencyDesc(benefit.frequencyDesc).toUpperCase());
                expect(element(page.view).element(page.plans.row(index)).element(page.benefits.row(key).column('item.maxIdr')).getText())
                  .toEqual(Number.parseFloat(benefit.maxIdr).toLocaleString());
              });
            } else {
              expect(element(page.view).element(page.plans.row(index)).element(page.benefits)
                .isPresent()).toBeFalsy();
            }
          });
        });
      } else {
        expect(element(page.view).element(page.plans).isPresent()).toBeFalsy();
      }
    });
  });
});
