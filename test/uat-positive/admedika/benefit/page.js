module.exports = {
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Benefit"]'),
  plans: by.repeater('plan in $.plans'),
  benefits: by.repeater('item in plan.benefits'),
  route: 'admedika/benefit'
};
