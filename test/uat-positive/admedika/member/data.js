var graphql = require('../../../lib/graphql');

module.exports = {
  getMember: function getMember(token) {
    var query = [
      'query Member {',
      '  profile {',
      '    _id',
      '    email',
      '    birthday',
      '    id',
      '    photo',
      '    admedika {',
      '      cardNumber',
      '      plans {',
      '        planType',
      '      }',
      '      member {',
      '        fullName',
      '        payorInfo',
      '        corporateInfo',
      '        policyNumber',
      '        dateOfBirth',
      '        memberId',
      '        memberType',
      '        planType',
      '        bpjsId',
      '        faskesLevel1',
      '        bpjsType',
      '        helpLine',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, {}, token);
  }
};
