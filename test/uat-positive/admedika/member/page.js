module.exports = {
  menu: {
    admedika: by.id('home-admedika'),
    benefit: by.id('header-benefit'),
    claim: by.id('header-claim'),
    provider: by.id('header-provider')
  },
  article: by.tagName('article'),
  profile: {
    id: by.id('member-id'),
    email: by.id('member-email'),
    photo: by.id('member-photo'),
    admedika: {
      cardNumber: by.id('member-card'),
      plans: by.id('member-benefit'),
      member: {
        fullName: by.id('member-fullname'),
        helpLine: by.partialButtonText('Call Center'),
        payorInfo: by.id('member-payor'),
        corporateInfo: by.id('member-corporate'),
        policyNumber: by.id('member-policy'),
        dateOfBirth: by.id('member-birthdate'),
        memberType: by.id('member-type'),
        bpjsId: by.id('member-bpjsId'),
        faskesLevel1: by.id('member-faskesLevel1'),
        bpjsType: by.id('member-bpjsType')
      }
    }
  },
  button: {
    card: by.partialButtonText('Show My Card')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Member Info"]'),
  route: 'admedika/member'
};
