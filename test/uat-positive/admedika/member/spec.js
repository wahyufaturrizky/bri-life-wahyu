var parseBenefits = require('../../../lib/parseBenefits');
var parseBirthdate = require('../../../lib/parseBirthdate');

var data = require('./data');
var page = require('./page');

describe(':: Member Info ::', function todo() {
  beforeAll(function before() {
    browser.setLocation('/');
  });

  it('should display member info', function be() {
    // click MyAdMedika menu
    browser.wait(element(page.menu.admedika).isDisplayed(), 5000, 'wait for MyAdMedika');
    browser.actions().mouseMove(element(page.menu.admedika)).click().perform();
    // open admedika member page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getMember(res);
    }).then(function resolve(res) {
      var profile = res && res.data && res.data.profile;
      if (profile) {
        // if (profile.photo) {
        //   browser.wait(element(page.view)
        //     .element(page.profile.photo).isDisplayed(), 5000, 'wait for photo');
        //   expect(element(page.view).element(page.profile.photo).getAttribute('src'))
        //     .toContain(profile.photo);
        // }
        expect(element(page.view).element(page.profile.admedika.member.helpLine).getAttribute('phone-call'))
          .toEqual(profile.admedika.member.helpLine || '');
        expect(element(page.view).element(page.profile.id).getText())
          .toEqual(profile.id);
        // expect(element(page.view).element(page.profile.email).getText())
        //   .toEqual(profile.email);
        expect(element(page.view).element(page.profile.admedika.cardNumber).getText())
          .toEqual(profile.admedika.cardNumber);
        expect(element(page.view).element(page.profile.admedika.plans).getText())
          .toEqual(parseBenefits(profile.admedika.plans));
        expect(element(page.view).element(page.profile.admedika.member.fullName).getText())
          .toEqual(profile.admedika.member.fullName);
        expect(element(page.view).element(page.profile.admedika.member.payorInfo).getText())
          .toEqual(profile.admedika.member.payorInfo);
        expect(element(page.view).element(page.profile.admedika.member.corporateInfo).getText())
          .toEqual(profile.admedika.member.corporateInfo);
        expect(element(page.view).element(page.profile.admedika.member.policyNumber).getText())
          .toEqual(profile.admedika.member.policyNumber);
        expect(element(page.view).element(page.profile.admedika.member.dateOfBirth).getText())
          .toEqual(parseBirthdate(profile.admedika.member.dateOfBirth || profile.birthday));
        expect(element(page.view).element(page.profile.admedika.member.memberType).getText())
          .toEqual(profile.admedika.member.memberType);
        expect(element(page.view).element(page.profile.admedika.member.bpjsId).getText())
          .toEqual(profile.admedika.member.bpjsId || '-');
        expect(element(page.view).element(page.profile.admedika.member.faskesLevel1).getText())
          .toEqual(profile.admedika.member.faskesLevel1 || '-');
        expect(element(page.view).element(page.profile.admedika.member.bpjsType).getText())
          .toEqual(profile.admedika.member.bpjsType || '-');
      } else {
        expect(element(page.view).element(page.article).isPresent())
          .toBeFalsy();
      }
    });
  });
});
