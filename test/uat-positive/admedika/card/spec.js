var parseBenefits = require('../../../lib/parseBenefits');
var parseBirthdate = require('../../../lib/parseBirthdate');

var getMember = require('../member/data').getMember;
var member = require('../member/page');

var page = require('./page');

describe(':: Show My Digital Card ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(member.route);
  });

  it('should display admedika card', function be() {
    element(member.view).element(member.article).isDisplayed().then(function is(displayed) {
      if (displayed) {
        // click show card button
        expect(element(member.view).element(member.button.card).isDisplayed()).toBeTruthy();
        browser.actions().mouseMove(element(member.view).element(member.button.card))
          .click().perform();
        // open card page
        browser.waitForAngular();
        expect(browser.getCurrentUrl()).toContain(page.route);
        //
        browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
          if (!(res)) return null;
          return getMember(res);
        }).then(function resolve(res) {
          var profile = res && res.data && res.data.profile;
          if (profile) {
            if (profile.photo) {
              browser.wait(element(page.view).element(page.profile.photo).isDisplayed(), 5000, 'wait for photo');
              expect(element(page.view).element(page.profile.photo).getAttribute('src'))
                .toContain(profile.photo);
            }
            if (profile.admedika.member.bpjsId) {
              expect(element(page.view).element(page.profile.admedika.member.bpjs).isDisplayed())
                .toBeTruthy();
              expect(element(page.view).element(page.profile.admedika.member.bpjsId).getText())
                .toEqual(profile.admedika.member.bpjsId);
            } else {
              expect(element(page.view).element(page.profile.admedika.member.bpjs).isDisplayed())
                .toBeFalsy();
              expect(element(page.view).element(page.profile.admedika.member.bpjsId).getText())
                .toEqual('-');
            }
            expect(element(page.view).element(page.profile.admedika.member.fullName).getText())
              .toEqual(profile.admedika.member.fullName);
            expect(element(page.view).element(page.profile.email).getText())
              .toEqual(profile.email);
            expect(element(page.view).element(page.profile.admedika.member.payorInfo).getText())
              .toEqual(profile.admedika.member.payorInfo);
            expect(element(page.view).element(page.profile.admedika.cardNumber).getText())
              .toEqual(profile.admedika.cardNumber);
            expect(element(page.view).element(page.profile.admedika.member.corporateInfo).getText())
              .toEqual(profile.admedika.member.corporateInfo);
            expect(element(page.view).element(page.profile.admedika.member.policyNumber).getText())
              .toEqual(profile.admedika.member.policyNumber);
            expect(element(page.view).element(page.profile.admedika.member.dateOfBirth).getText())
              .toEqual(parseBirthdate(profile.admedika.member.dateOfBirth || profile.birthday));
            expect(element(page.view).element(page.profile.admedika.member.memberType).getText())
              .toEqual(profile.admedika.member.memberType);
            expect(element(page.view).element(page.profile.admedika.plans).getText())
              .toEqual(parseBenefits(profile.admedika.plans));
            expect(element(page.view).element(page.profile.admedika.member.faskesLevel1).getText())
              .toEqual(profile.admedika.member.faskesLevel1 || '-');
            expect(element(page.view).element(page.profile.admedika.member.bpjsType).getText())
              .toEqual(profile.admedika.member.bpjsType || '-');
            expect(element(page.view).element(page.barcode).getAttribute('string'))
              .toEqual(profile.admedika.cardNumber);
            expect(element(page.view).element(page.qrcode).getAttribute('data'))
              .toEqual(profile.admedika.cardNumber);
          } else {
            expect(element(page.view).element(page.article).isPresent())
              .toBeFalsy();
          }
          // click close icon
          browser.wait(element(page.view).element(page.button.close).isDisplayed(), 5000, 'wait for close');
          browser.actions().mouseMove(element(page.view).element(page.button.close))
            .click().perform();
        });
      }
    });
  });
});
