module.exports = {
  article: by.tagName('article'),
  profile: {
    email: by.id('card-email'),
    photo: by.id('card-photo'),
    admedika: {
      cardNumber: by.id('card-number'),
      plans: by.id('card-benefit'),
      member: {
        fullName: by.id('card-fullname'),
        payorInfo: by.id('card-payor'),
        corporateInfo: by.id('card-corporate'),
        policyNumber: by.id('card-policy'),
        dateOfBirth: by.id('card-birthdate'),
        memberType: by.id('card-type'),
        bpjs: by.id('card-bpjs'),
        bpjsId: by.id('card-bpjsId'),
        faskesLevel1: by.id('card-faskesLevel1'),
        bpjsType: by.id('card-bpjsType')
      }
    }
  },
  button: {
    close: by.id('card-close')
  },
  barcode: by.tagName('barcode'),
  qrcode: by.tagName('qrcode'),
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Show My Digital Card"]'),
  route: 'admedika/show'
};
