module.exports = {
  input: {
    city: by.id('provider-city'),
    benefit: by.id('provider-benefit'),
    match: by.model('filter.match'),
    position: by.id('provider-position')
  },
  button: {
    submit: by.buttonText('Submit'),
    reset: by.id('provider-reset'),
    sort: by.id('provider-sort'),
    near: by.id('provider-near')
  },
  popup: {
    container: by.css('.popup-container.popup-showing.active'),
    close: by.buttonText('Tutup'),
    searchError: by.id('search-error')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Provider"]'),
  groupProviders: by.repeater('(key, values) in $.groupProviders'),
  providers: by.repeater('item in values'),
  cities: by.repeater('city in $.profile.admedika.cities'),
  benefits: by.repeater('benefit in $.profile.admedika.plans'),
  route: 'admedika/provider'
};
