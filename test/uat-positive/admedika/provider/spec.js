var groupByAlpha = require('../../../lib/groupByAlpha');
var titleize = require('../../../lib/titleize');

var member = require('../member/page');

var data = require('./data');
var page = require('./page');

function randomIndex(max) {
  return Math.floor((Math.random() * max)) || 0;
}

function validateProvider(item) {
  Object.keys(item).forEach(function each(key, index) {
    expect(element(page.view).element(page.groupProviders.row(index).column('key')).getText()).toEqual(key.toUpperCase());
    if (item[key].length) {
      item[key].forEach(function every(provider, row) {
        if (provider.distance) {
          expect(element(page.view).element(page.groupProviders.row(index)).element(page.providers.row(row).column('item.distance.text')).getText())
            .toEqual(provider.distance.text || (provider.distance.value ? provider.distance.value + ' km' : provider.distance.value));
        }
        expect(element(page.view).element(page.groupProviders.row(index)).element(page.providers.row(row).column('item.providerName')).getText())
          .toEqual(provider.providerName);
        expect(element(page.view).element(page.groupProviders.row(index)).element(page.providers.row(row).column('item.providerId')).getText())
          .toEqual(provider.providerId);
      });
    } else {
      expect(element(page.view).element(page.providers).isPresent()).toBeFalsy();
    }
  });
}

describe(':: Provider ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(member.route);
    // click provider menu
    expect(element(member.view).element(member.menu.provider).isDisplayed()).toBeTruthy();
    browser.actions().mouseMove(element(member.view).element(member.menu.provider))
      .click().perform();
    // open provider page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
  });

  it('should show list of providers', function be() {
    browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getProviders(res);
    }).then(function resolve(res) {
      if (res && res.length) {
        validateProvider(groupByAlpha(res));
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of providers by city', function be() {
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getCityProviders(res);
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      var city;
      if (item && item.cities && item.cities.length) {
        expect(element(page.view).element(page.cities).isPresent()).toBeTruthy();
        expect(element(page.view).all(page.cities).getText())
          .toEqual(item.cities.map(function each(val) {
            return val && titleize(val.name);
          }).filter(Boolean));
        // select city
        element(page.view).all(page.cities).then(function select(options) {
          var index = randomIndex(options.length);
          options[index].click();
          options[index].getAttribute('value').then(function opts(val) {
            city = val;
          });
        });
        // click submit
        browser.actions().mouseMove(element(page.view).element(page.button.submit))
          .click().perform();
        browser.waitForAngular();
        browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
          if (!(token)) return null;
          return data.getProviders(token, { providerCity: city });
        }).then(function success(result) {
          if (result && result.length) {
            validateProvider(groupByAlpha(result));
          } else {
            expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
          }
        });
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of providers by benefit', function be() {
    // select city
    element(page.view).element(page.input.city).click();
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getBenefitProviders(res);
    }).then(function resolve(res) {
      var item = res && res.data && res.data.profile && res.data.profile.admedika;
      var benefit;
      if (item && item.plans && item.plans.length) {
        expect(element(page.view).element(page.benefits).isPresent()).toBeTruthy();
        expect(element(page.view).all(page.benefits).getText())
          .toEqual(item.plans.map(function each(val) {
            return val && titleize(val.planType);
          }).filter(Boolean));
        // select benefit
        element(page.view).all(page.benefits).then(function select(options) {
          var index = randomIndex(options.length);
          options[index].click();
          options[index].getAttribute('value').then(function opts(val) {
            benefit = val;
          });
        });
        // click submit
        browser.actions().mouseMove(element(page.view).element(page.button.submit))
          .click().perform();
        browser.waitForAngular();
        browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
          if (!(token)) return null;
          return data.getProviders(token, { planId: benefit });
        }).then(function success(result) {
          if (result && result.length) {
            validateProvider(groupByAlpha(result));
          } else {
            expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
          }
        });
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of providers by city + benefit', function be() {
    var city;
    var benefit;
    // select city
    element(page.view).element(page.cities).isPresent().then(function resolve(cities) {
      if (cities) {
        element(page.view).all(page.cities).then(function select(options) {
          var index = randomIndex(options.length);
          options[index].click();
          options[index].getAttribute('value').then(function opts(val) {
            city = val;
          });
        });
        // select benefit
        element(page.view).element(page.benefits).isPresent().then(function success(benefits) {
          if (benefits) {
            element(page.view).all(page.benefits).then(function select(options) {
              var index = randomIndex(options.length);
              options[index].click();
              options[index].getAttribute('value').then(function opts(val) {
                benefit = val;
              });
            });
            // click submit
            browser.waitForAngular();
            browser.actions().mouseMove(element(page.view).element(page.button.submit))
              .click().perform();
            browser.waitForAngular();
            browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function result(token) {
              if (!(token)) return null;
              return data.getProviders(token, { providerCity: city, planId: benefit });
            }).then(function result(res) {
              if (res && res.length) {
                validateProvider(groupByAlpha(res));
              } else {
                expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
              }
            });
          }
        });
      }
    });
  });

  it('should show list of providers by search', function be() {
    var keyword = 'medika';
    // select city
    element(page.view).element(page.input.city).click();
    // select benefit
    element(page.view).element(page.input.benefit).click();
    // input field search
    element(page.view).element(page.input.match).clear().sendKeys(keyword);
    // click submit
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    browser.waitForAngular();
    browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
      if (!(token)) return null;
      return data.getProviders(token, { match: keyword });
    }).then(function success(result) {
      if (result && result.length) {
        validateProvider(groupByAlpha(result));
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of providers by reset', function be() {
    // click reset
    browser.actions().mouseMove(element(page.view).element(page.button.reset)).click().perform();
    browser.waitForAngular();
    browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
      if (!(token)) return null;
      return data.getProviders(token);
    }).then(function success(result) {
      if (result && result.length) {
        validateProvider(groupByAlpha(result));
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
  });

  it('should show list of providers by sort', function be() {
    // click sort
    browser.actions().mouseMove(element(page.view).element(page.button.sort)).click().perform();
    browser.waitForAngular();
    browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
      if (!(token)) return null;
      return data.getProviders(token, { sort: '-providerName' });
    }).then(function success(result) {
      if (result && result.length) {
        validateProvider(groupByAlpha(result));
      } else {
        expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
      }
    });
    // click sort
    browser.actions().mouseMove(element(page.view).element(page.button.sort)).click().perform();
  });

  it('should show list of providers by near me', function be() {
    // click near
    browser.actions().mouseMove(element(page.view).element(page.button.near)).click().perform();
    browser.driver.sleep(20000);
    browser.waitForAngular();
    element(page.view).element(page.input.position).getAttribute('alt').then(function resolve(res) {
      if (res) {
        browser.executeScript('return window.localStorage.getItem(\'providers\');').then(function success(token) {
          if (!(token)) return null;
          return data.getProviders(token, { nearMe: res });
        }).then(function success(result) {
          if (result && result.length) {
            validateProvider(groupByAlpha(result, true));
          } else {
            expect(element(page.view).element(page.groupProviders).isPresent()).toBeFalsy();
          }
        });
      }
    });
  });
});
