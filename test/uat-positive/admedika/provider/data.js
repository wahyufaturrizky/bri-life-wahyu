var _ = require('underscore');
var geolib = require('geolib');

var graphql = require('../../../lib/graphql');
var parseSortProvider = require('../../../lib/parseSortProvider');

module.exports = {
  getBenefitProviders: function getBenefitProviders(token) {
    var variables = { sortPlan: 'planType' };
    var query = [
      'query Cities($sortPlan: String) {',
      '  profile {',
      '    admedika {',
      '      plans(sort: $sortPlan) {',
      '        planId',
      '        planType',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  },
  getCityProviders: function getCityProviders(token) {
    var variables = { sortCity: 'name' };
    var query = [
      'query Cities($sortCity: String) {',
      '  profile {',
      '    admedika {',
      '      cities(sort: $sortCity) {',
      '        name',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  },
  getProviders: function getProviders(token, params) {
    var options = params || {};
    var variables = {
      sort: options.sort || 'providerName',
      skip: 0,
      limit: 10,
      providerCity: options.providerCity || '',
      planId: options.planId || '*',
      match: options.match || '',
      nearMe: options.nearMe || ''
    };

    return protractor.promise.when(token)
      .then(function resolve(res) {
        return JSON.parse(res) || [];
      })
      .then(function resolve(res) {
        return _.reject(res, function c(o) {
          return o.providerName === '-' || /error/gi.test(o.providerName);
        });
      })
      .then(function resolve(res) {
        var desc = _.first(variables.sort) === '-';
        var sort = desc ? variables.sort.slice(1) : variables.sort.slice(0);
        var data = _.sortBy(res, function c(o) {
          return parseSortProvider(o[sort].replace(/[^a-z& ]/gi, '').trim());
        });
        if (desc) return data.reverse();
        return data;
      })
      .then(function resolve(res) {
        if (variables.providerCity && !(variables.providerCity === '')) {
          return _.filter(res, function c(o) {
            return (new RegExp('^' + variables.providerCity + '$', 'gi')).test(o.providerCity);
          });
        }
        return protractor.promise.fulfilled(res);
      })
      .then(function resolve(res) {
        var pattern;
        if (variables.match) {
          pattern = new RegExp(variables.match, 'gi');
          return _.filter(res, function c(o) {
            return pattern.test(o.providerId) || pattern.test(o.providerName) ||
              pattern.test(o.providerPhoneNum);
          });
        }
        return protractor.promise.fulfilled(res);
      })
      .then(function resolve(res) {
        if (variables.planId && !(variables.planId === '*')) {
          return _.filter(res, function c(o) {
            return o.plans.length && _.findWhere(o.plans, { planId: variables.planId });
          });
        }
        return protractor.promise.fulfilled(res);
      })
      .then(function resolve(res) {
        var position;
        var collections;

        if (variables.nearMe) {
          position = variables.nearMe.split(',');
          collections = _.filter(res, function c(o) {
            return o.latitude && o.longitude &&
              !(Number.parseFloat(o.latitude) === 0 && Number.parseFloat(o.longitude) === 0);
          });

          collections.forEach(function c(o, e) {
            collections[e].distance = {};
            collections[e].distance = {
              value: geolib.convertUnit('km', geolib.getDistance({
                latitude: position[0], longitude: position[1]
              }, {
                latitude: o.latitude, longitude: o.longitude
              }), 2)
            };
          });

          collections = _.filter(collections, function c(o) {
            return o.distance && o.distance.value;
          });

          collections = _.sortBy(collections, function c(o) {
            return Number.parseFloat(o.distance.value);
          });

          if (_.first(variables.sort) === '-') return collections.reverse();
          return collections;
        }
        return protractor.promise.fulfilled(res);
      })
      .then(function resolve(res) {
        return protractor.promise.fulfilled(res.splice(variables.skip, variables.limit));
      })
      .catch(function reject(err) {
        return protractor.promise.rejectted(err);
      });
  },
  _getProviders: function _getProviders(token, params) {
    var options = params || {};
    var variables = {
      sort: options.sort || 'providerName',
      skip: 0,
      limit: 10,
      providerCity: options.providerCity || '',
      planId: options.planId || '*',
      match: options.match || '',
      nearMe: options.nearMe || ''
    };
    var query = [
      'query Provider($planId: String, $providerCity: String, $match: String, $nearMe: String, $sort: String, $skip: Int, $limit: Int) {',
      '  profile {',
      '    admedika {',
      '      providers(planId: $planId, providerCity: $providerCity, match: $match, nearMe: $nearMe, sort: $sort, skip: $skip, limit: $limit) {',
      '        providerId',
      '        providerName',
      '        providerCity',
      '        providerAddress',
      '        providerPhoneNum',
      '        longitude',
      '        latitude',
      '        distance {',
      '          text',
      '          value',
      '        }',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  }
};
