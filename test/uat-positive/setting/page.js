module.exports = {
  button: {
    sidemenu: by.id('sidemenu'),
    setting: by.id('setting')
  },
  view: by.css('ion-side-menus[nav-view="active"] [nav-bar="active"]'),
  sidebar: by.className('menu-open'),
  sidemenu: {
    setting: by.id('sidemenu-setting')
  },
  route: 'setting'
};
