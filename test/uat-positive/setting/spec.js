var page = require('./page');

describe(':: Setting ::', function todo() {
  beforeAll(function before() {
    browser.setLocation('/');
  });

  it('should display setting page', function be() {
    // click sidemenu toggle
    browser.wait(element(page.view).element(page.button.sidemenu).isDisplayed(), 5000, 'wait for sidemenu');
    browser.actions().mouseMove(element(page.view).element(page.button.sidemenu)).click().perform();
    // show sidemenu
    expect(element(page.sidebar).isPresent()).toBeTruthy();
    // click setting menu
    browser.wait(element(page.sidemenu.setting).isDisplayed(), 5000, 'wait for setting');
    browser.actions().mouseMove(element(page.sidemenu.setting)).click().perform();
    // open setting page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
  });
});
