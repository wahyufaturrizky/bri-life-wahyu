module.exports = {
  input: {
    password: by.name('password'),
    passwordConfirmation: by.name('passwordConfirmation')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Change Password"]'),
  button: {
    submit: by.css('button[type="submit"]')
  },
  route: 'profile/change-password'
};
