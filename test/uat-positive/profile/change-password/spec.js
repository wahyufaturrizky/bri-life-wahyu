var reverse = require('../../../lib/reverse');

var data = require('../../authentication/login/data');
var login = require('../../authentication/login/page');

var profile = require('../show/page');

var page = require('./page');

describe(':: Change Password ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(profile.route);
  });

  it('should change password', function be() {
    var password = reverse(data.password);
    // click edit password button
    browser.wait(element(profile.button.changePassword).isDisplayed(), 5000, 'wait for edit password');
    browser.actions().mouseMove(element(profile.button.changePassword)).click().perform();
    // open change password page
    expect(browser.getCurrentUrl()).toContain(page.route);
    // input field password
    element(page.view).element(page.input.password).clear().sendKeys(password);
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear().sendKeys(password);
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isEnabled(), 5000, 'wait for submit');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // open login page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(login.route);
    // input field username
    element(login.input.username).clear().sendKeys(data.email);
    // input field password
    element(login.input.password).clear().sendKeys(password);
    // click login button
    browser.wait(element(login.button.submit).isDisplayed(), 5000, 'wait for login');
    browser.actions().mouseMove(element(login.button.submit)).click().perform();
    // open home page
    browser.getTitle().then(function resolve(res) {
      expect(res).toMatch(/home/i);
    });
  });

  afterAll(function after() {
    var password = data.password;
    // open change password page
    browser.setLocation(page.route);
    // input field password
    element(page.view).element(page.input.password).clear().sendKeys(password);
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear().sendKeys(password);
    // click submit
    expect(element(page.view).element(page.button.submit).isEnabled()).toBeTruthy();
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // open login page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(login.route);
  });
});
