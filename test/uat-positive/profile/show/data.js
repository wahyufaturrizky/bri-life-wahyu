var graphql = require('../../../lib/graphql');

module.exports = {
  getProfile: function getProfile(token) {
    var query = [
      'query Profile {',
      '  profile {',
      '    _id',
      '    birthday',
      '    name',
      '    email',
      '    phone',
      '    id',
      '    photo',
      '    admedika {',
      '      cardNumber',
      '      member {',
      '        bpjsId',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('');
    return graphql(query, {}, token);
  }
};
