var parseBirthdate = require('../../../lib/parseBirthdate');
var titleize = require('../../../lib/titleize');

var setting = require('../../setting/page');

var data = require('./data');
var page = require('./page');

describe(':: Show My Profile ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(setting.route);
  });

  it('should go to profile page', function be() {
    // click profile menu
    browser.wait(element(page.menu).isDisplayed(), 5000, 'wait for profile');
    browser.actions().mouseMove(element(page.menu)).click().perform();
    // open profile page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getProfile(res);
    }).then(function resolve(res) {
      var profile = res && res.data && res.data.profile;
      if (profile) {
        // if (profile.photo) {
        //   browser.wait(element(page.view)
        //     .element(page.profile.photo).isDisplayed(), 5000, 'wait for photo');
        //   expect(element(page.view).element(page.profile.photo).getAttribute('src'))
        //     .toContain(profile.photo);
        // }
        if (profile.birthday) {
          expect(element(page.view).element(page.profile.birthday).getText())
            .toEqual(parseBirthdate(profile.birthday));
        } else {
          expect(element(page.view).element(page.profile.birthday).getText())
            .toEqual('-');
        }
        if (profile.phone) {
          expect(element(page.view).element(page.profile.phone).getText())
            .toEqual(profile.phone);
        } else {
          expect(element(page.view).element(page.profile.phone).getText())
            .toEqual('-');
        }
        if (profile.admedika.cardNumber) {
          expect(element(page.view).element(page.profile.admedika.connect).isDisplayed())
            .toBeFalsy();
          expect(element(page.view).element(page.profile.admedika.cardNumber).getText())
            .toEqual(profile.admedika.cardNumber);
        } else {
          expect(element(page.view).element(page.profile.admedika.connect).isDisplayed())
            .toBeTruthy();
        }
        if (profile.admedika.member.bpjsId) {
          expect(element(page.view).element(page.profile.admedika.member.bpjsId).getText())
            .toEqual(profile.admedika.member.bpjsId);
        } else {
          expect(element(page.view).element(page.profile.admedika.member.bpjsId).getText())
            .toEqual('-');
        }
        expect(element(page.view).element(page.profile.id).getText())
          .toEqual(profile.id);
        expect(element(page.view).element(page.profile.email).getText())
          .toEqual(profile.email);
        expect(element(page.view).element(page.profile.name).getText())
          .toEqual(titleize(profile.name));
      } else {
        expect(element(page.view).element(page.article).isPresent())
          .toBeFalsy();
      }
    });
  });
});
