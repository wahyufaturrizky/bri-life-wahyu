module.exports = {
  menu: by.id('setting-profile'),
  article: by.tagName('article'),
  profile: {
    id: by.id('profile-id'),
    birthday: by.id('profile-birthdate'),
    email: by.id('profile-email'),
    name: by.id('profile-name'),
    phone: by.id('profile-phone'),
    photo: by.id('profile-photo'),
    admedika: {
      connect: by.id('profile-connect'),
      cardNumber: by.id('profile-card'),
      member: {
        bpjsId: by.id('profile-bpjsId')
      }
    }
  },
  button: {
    changePassword: by.id('profile-edit-password')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="My Profile"]'),
  route: 'profile/mine'
};
