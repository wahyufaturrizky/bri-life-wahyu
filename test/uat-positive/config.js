'use strict';

var wd = require('wd');
var protractor = require('protractor');
var wdBridge = require('wd-bridge')(protractor, wd);
var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
var path = require('path');

exports.config = {
  framework: 'jasmine',
  /*
   * normally protractor runs tests on localhost:4444, but we want protractor to connect to appium
   * which runs on localhost:4723
   */
  seleniumAddress: 'http://localhost:4723/wd/hub',

  specs: [
    './**/register/spec.js',
    './**/activate/spec.js',
    './**/forget-password/spec.js',
    './**/forget-password-success/spec.js',
    './**/login/spec.js',
    './**/member/spec.js',
    './**/card/spec.js',
    './**/benefit/spec.js',
    './**/claim/spec.js',
    './**/provider/spec.js',
    './**/setting/spec.js',
    './**/terms-condition/spec.js',
    './**/faq/spec.js',
    './**/about/spec.js',
    './**/contact-us/spec.js',
    './**/show/spec.js',
    './**/change-password/spec.js',
    './**/logout/spec.js'
  ],

  /*
   * Some capabilities must be set to make sure appium can connect to your device.
   * platformVersion: this is the version of android
   * deviceName: your actual device name
   * browserName: leave this empty, we want protractor to use the embedded webview
   * autoWebView: set this to true for hybrid applications
   * app: the location of the apk (must be absolute)
   */
  multiCapabilities: [
    {
      platformName: 'Android',
      platformVersion: '6.0.1',
      deviceName: 'Galaxy J5',
      browserName: '',
      autoWebview: true,
      app: path.resolve(__dirname, '..', '..', 'platforms', 'android', 'build', 'outputs', 'apk', 'android-debug.apk')
    }
  ],

  baseUrl: '',

  allScriptsTimeout: 120000,

  /* configuring wd in onPrepare
   * wdBridge helps to bridge wd driver with other selenium clients
   * See https://github.com/sebv/wd-bridge/blob/master/README.md
   */
  onPrepare: function onPrepare() {
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: false } }));
    wdBridge.initFromProtractor(exports.config);
  },

  jasmineNodeOpts: {
    defaultTimeoutInterval: 120000,
    print: function print() {
      //
    }
  }
};
