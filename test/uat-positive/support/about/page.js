module.exports = {
  menu: by.id('setting-about'),
  about: {
    title: by.id('about-title'),
    subtitle: by.id('about-subtitle'),
    content: by.id('about-content')
  },
  article: by.tagName('article'),
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="About"]'),
  route: 'support/about'
};
