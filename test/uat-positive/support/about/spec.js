var parseHtml = require('../../../lib/parseHtml');

var setting = require('../../setting/page');

var data = require('./data');
var page = require('./page');

describe(':: About ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(setting.route);
  });

  it('should go to about page', function be() {
    // click about menu
    browser.wait(element(page.menu).isDisplayed(), 5000, 'wait for terms');
    browser.actions().mouseMove(element(page.menu)).click().perform();
    // open about page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getAbout(res);
    }).then(function resolve(res) {
      var about = res && res.data && res.data.supports && res.data.supports.length;
      if (about) {
        about = res.data.supports.pop();
        if (about.title) {
          expect(element(page.about.title).getText()).toEqual(about.title);
        } else {
          expect(element(page.about.title).getText()).toEqual('');
        }
        if (about.subtitle) {
          expect(element(page.view).element(page.about.subtitle).getText()).toEqual(about.subtitle);
        } else {
          expect(element(page.view).element(page.about.subtitle).isDisplayed()).toBeFalsy();
        }
        expect(element(page.view).element(page.about.content).getAttribute('innerHTML')).toEqual(parseHtml(about.content));
      } else {
        expect(element(page.view).element(page.article).isPresent()).toBeFalsy();
      }
    });
  });
});
