module.exports = {
  menu: by.id('setting-faq'),
  faq: {
    title: by.id('faq-title'),
    subtitle: by.id('faq-subtitle'),
    content: by.id('faq-content')
  },
  article: by.tagName('article'),
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="FAQ"]'),
  route: 'support/faq'
};
