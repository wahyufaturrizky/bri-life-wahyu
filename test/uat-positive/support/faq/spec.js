var parseHtml = require('../../../lib/parseHtml');

var setting = require('../../setting/page');

var data = require('./data');
var page = require('./page');

describe(':: FAQ ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(setting.route);
  });

  it('should go to faq page', function be() {
    // click faq menu
    browser.wait(element(page.menu).isDisplayed(), 5000, 'wait for terms');
    browser.actions().mouseMove(element(page.menu)).click().perform();
    // open faq page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getFaq(res);
    }).then(function resolve(res) {
      var faq = res && res.data && res.data.supports && res.data.supports.length;
      if (faq) {
        faq = res.data.supports.pop();
        if (faq.title) {
          expect(element(page.faq.title).getText()).toEqual(faq.title);
        } else {
          expect(element(page.faq.title).getText()).toEqual('FAQ');
        }
        if (faq.subtitle) {
          expect(element(page.view).element(page.faq.subtitle).getText()).toEqual(faq.subtitle);
        } else {
          expect(element(page.view).element(page.faq.subtitle).isDisplayed()).toBeFalsy();
        }
        expect(element(page.view).element(page.faq.content).getAttribute('innerHTML')).toEqual(parseHtml(faq.content));
      } else {
        expect(element(page.view).element(page.article).isPresent()).toBeFalsy();
      }
    });
  });
});
