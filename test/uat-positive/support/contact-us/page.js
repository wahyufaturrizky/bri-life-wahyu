module.exports = {
  menu: by.id('setting-contact'),
  input: {
    content: by.model('data.content')
  },
  button: {
    submit: by.css('button[type="submit"]')
  },
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Contact Us / Inquiry"]'),
  route: 'support/contact-us'
};
