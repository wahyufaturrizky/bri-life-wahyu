var setting = require('../../setting/page');

var data = require('./data');
var page = require('./page');

describe(':: Contact Us ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(setting.route);
  });

  it('should send inquiry', function be() {
    // click contact menu
    browser.wait(element(page.menu).isDisplayed(), 5000, 'wait for contact');
    browser.actions().mouseMove(element(page.menu)).click().perform();
    // open contact page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    // input field content
    element(page.view).element(page.input.content).clear().sendKeys(data.inquiry);
    // click submit button
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    browser.waitForAngular();
  });
});
