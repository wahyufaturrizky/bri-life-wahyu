module.exports = {
  menu: by.id('setting-terms'),
  terms: {
    title: by.id('terms-title'),
    subtitle: by.id('terms-subtitle'),
    content: by.id('terms-content')
  },
  article: by.tagName('article'),
  view: by.css('ion-side-menus[nav-view="active"] ion-view[nav-view="active"][view-title="Terms & Condition"]'),
  route: 'support/terms-condition'
};
