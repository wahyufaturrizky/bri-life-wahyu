var graphql = require('../../../lib/graphql');

module.exports = {
  getTerms: function getTerms(token) {
    var variables = {
      limit: 1,
      match: JSON.stringify({ name: { $regex: 'terms-condition', $options: 'i' } })
    };
    var query = [
      'query Supports($match: String, $limit: Int) {',
      '  supports(match: $match, limit: $limit) {',
      '    _id',
      '    title',
      '    subtitle',
      '    content',
      '  }',
      '}'
    ].join('');
    return graphql(query, variables, token);
  }
};
