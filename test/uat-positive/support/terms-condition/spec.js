var parseHtml = require('../../../lib/parseHtml');

var setting = require('../../setting/page');

var data = require('./data');
var page = require('./page');

describe(':: Terms & Condition ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(setting.route);
  });

  it('should go to terms page', function be() {
    // click terms menu
    browser.wait(element(page.menu).isDisplayed(), 5000, 'wait for terms');
    browser.actions().mouseMove(element(page.menu)).click().perform();
    // open terms page
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain(page.route);
    //
    browser.executeScript('return window.localStorage.getItem(\'token\');').then(function resolve(res) {
      if (!(res)) return null;
      return data.getTerms(res);
    }).then(function resolve(res) {
      var terms = res && res.data && res.data.supports && res.data.supports.length;
      if (terms) {
        terms = res.data.supports.pop();
        if (terms.title) {
          expect(element(page.terms.title).getText()).toEqual(terms.title);
        } else {
          expect(element(page.terms.title).getText()).toEqual('Terms & Condition');
        }
        if (terms.subtitle) {
          expect(element(page.view).element(page.terms.subtitle).getText()).toEqual(terms.subtitle);
        } else {
          expect(element(page.view).element(page.terms.subtitle).isDisplayed()).toBeFalsy();
        }
        expect(element(page.view).element(page.terms.content).getAttribute('innerHTML')).toEqual(parseHtml(terms.content));
      } else {
        expect(element(page.view).element(page.article).isPresent()).toBeFalsy();
      }
    });
  });
});
