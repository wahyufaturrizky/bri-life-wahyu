var page = require('../../../uat-positive/authentication/activate/page');

describe(':: Activation ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
    browser.setLocation(page.route);
  });

  it('should alert failed activation', function be() {
    element(page.view).element(page.input.code).clear().sendKeys('TEST');
    // click submit button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for submit');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.driver.sleep(2000);
    browser.waitForAngular();
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // click close
    browser.wait(element(page.popup.close).isPresent(), 5000, 'wait for close');
    browser.actions().mouseMove(element(page.popup.close)).click().perform();
  });
});
