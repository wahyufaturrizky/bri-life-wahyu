var data = require('../../../uat-positive/authentication/login/data');
var page = require('../../../uat-positive/authentication/login/page');

var reverse = require('../../../lib/reverse');

describe(':: Login ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
    browser.setLocation(page.route);
  });

  it('should alert required credentials', function be() {
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for login');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    expect(element(page.popup.noCredential).isPresent()).toBeTruthy();
    // click close
    browser.wait(element(page.popup.close).isPresent(), 5000, 'wait for close');
    browser.actions().mouseMove(element(page.popup.close)).click().perform();
  });

  it('should alert failed login', function be() {
    // input field username
    element(page.view).element(page.input.username).clear().sendKeys(data.email);
    // input field password
    element(page.view).element(page.input.password).clear().sendKeys(reverse(data.password));
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for login');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.waitForAngular();
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    expect(element(page.popup.loginError).isPresent()).toBeTruthy();
    // click close
    browser.wait(element(page.popup.close).isPresent(), 5000, 'wait for close');
    browser.actions().mouseMove(element(page.popup.close)).click().perform();
  });
});
