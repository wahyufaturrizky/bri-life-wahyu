var data = require('../../../uat-positive/authentication/register/data');
var page = require('../../../uat-positive/authentication/register/page');

var login = require('../../../uat-positive/authentication/login/data');

var randomIndex = require('../../../lib/randomIndex');
var reverse = require('../../../lib/reverse');

describe(':: Register ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
    browser.setLocation(page.route);
  });

  it('should alert required name', function be() {
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi nama/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required birthday', function be() {
    // input field name
    element(page.view).element(page.input.name).clear().sendKeys(data.name);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi tanggal lahir/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required gender', function be() {
    // input field name
    element(page.view).element(page.input.name).clear().sendKeys(data.name);
    // click field birthday
    browser.actions().mouseMove(element(page.view).element(page.input.birthday)).click().perform();
    // show popup datepicker
    browser.wait(element(page.datepicker.popup).isDisplayed(), 5000, 'wait for datepicker popup');
    // select datepicker month
    browser.wait(element(page.datepicker.month).isPresent(), 5000, 'wait for datepicker month');
    element.all(page.datepicker.month).then(function resolve(options) {
      options[randomIndex((new Date()).getMonth())].click();
    });
    // select datepicker year
    browser.wait(element(page.datepicker.year).isPresent(), 5000, 'wait for datepicker year');
    element.all(page.datepicker.year).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // select datepicker date
    browser.wait(element(page.datepicker.day).isPresent(), 5000, 'wait for datepicker date');
    element.all(page.datepicker.day).then(function resolve(options) {
      options[randomIndex(options.length)].click();
    });
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi jenis kelamin/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required phone', function be() {
    // click field gender
    browser.wait(element(page.view).element(page.input.gender).isDisplayed(), 5000, 'wait for gender field');
    element.all(page.genderpicker).then(function resolve(options) {
      // select gender
      options[randomIndex(options.length)].click();
    });
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi nomor handphone/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required email', function be() {
    // input field phone
    browser.wait(element(page.view).element(page.input.phone).isDisplayed(), 5000, 'wait for phone field');
    element(page.view).element(page.input.phone).clear().sendKeys(data.phone.concat('TEST'));
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi email/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required password', function be() {
    // input field email
    element(page.view).element(page.input.email).clear().sendKeys(data.email);
    // input field card
    element(page.view).element(page.input.cardNumber).clear().sendKeys(data.cardNumber.concat('96'));
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi password/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required password confirmation', function be() {
    // input field password
    element(page.view).element(page.input.password).clear()
      .sendKeys(element(page.captcha).getText());
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/isi konfirmasi password/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert required captcha', function be() {
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear()
      .sendKeys(reverse(data.password));
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/kode belum diisi/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert numeric phone', function be() {
    // input field code
    element(page.view).element(page.input.code).clear().sendKeys('0T35T');
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/berupa angka/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert size of phone', function be() {
    // input field phone
    element(page.view).element(page.input.phone).clear().sendKeys(data.cardNumber);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/12 karakter/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert minimum password', function be() {
    // input field phone
    element(page.view).element(page.input.phone).clear().sendKeys(data.phone);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/minimal 6 karakter/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert invalid password confirmation', function be() {
    // input field password confirmation
    element(page.view).element(page.input.password).clear().sendKeys(data.password);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/tidak cocok/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert invalid captcha', function be() {
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear().sendKeys(data.password);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/kode salah/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert size of card number', function be() {
    // input field code
    element(page.view).element(page.input.code).clear().sendKeys(element(page.captcha).getText());
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/16 digit/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert numeric card number', function be() {
    // input field card
    element(page.view).element(page.input.cardNumber).clear().sendKeys(data.cardNumber.concat('L'));
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // validate popup
    expect(element(page.popup.message).getText()).toMatch(/berupa angka/i);
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert failed register by card number', function be() {
    // input field card
    element(page.view).element(page.input.cardNumber).clear().sendKeys(data.cardNumber.concat('7'));
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show modal agreement
    browser.waitForAngular();
    browser.wait(element(page.agreement.modal).isDisplayed(), 5000, 'wait for agreement');
    expect(element(page.agreement.disagree).isPresent()).toBeTruthy();
    // click agree
    browser.wait(element(page.agreement.agree).isPresent(), 5000, 'wait for agree');
    browser.actions().mouseMove(element(page.agreement.agree)).click().perform();
    // show popup error
    browser.waitForAngular();
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });

  it('should alert failed register by email', function be() {
    // input field card
    element(page.view).element(page.input.cardNumber).clear();
    // input field email
    element(page.view).element(page.input.email).clear().sendKeys(login.email);
    // click register button
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for register');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.waitForAngular();
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // click ok
    browser.wait(element(page.popup.ok).isPresent(), 5000, 'wait for ok');
    browser.actions().mouseMove(element(page.popup.ok)).click().perform();
  });
});
