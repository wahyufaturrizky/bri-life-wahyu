var page = require('../../../uat-positive/authentication/forget-password/page');
var register = require('../../../uat-positive/authentication/register/data');

describe(':: Forget Password ::', function todo() {
  beforeAll(function before() {
    browser.executeScript('window.localStorage.clear();');
    browser.setLocation(page.route);
  });

  it('should alert failed reset password', function be() {
    // input field email
    element(page.view).element(page.input.email).clear().sendKeys(register.email.concat('ops'));
    // click submit
    browser.wait(element(page.view).element(page.button.submit).isDisplayed(), 5000, 'wait for submit');
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    // show popup error
    browser.waitForAngular();
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    // click close
    browser.wait(element(page.popup.close).isPresent(), 5000, 'wait for close');
    browser.actions().mouseMove(element(page.popup.close)).click().perform();
  });
});
