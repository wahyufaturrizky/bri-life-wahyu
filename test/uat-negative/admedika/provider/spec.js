var page = require('../../../uat-positive/admedika/provider/page');

describe(':: Provider ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(page.route);
  });

  it('should search not found', function be() {
    // input field search
    element(page.view).element(page.input.match).clear().sendKeys('534rcH');
    // click submit
    browser.actions().mouseMove(element(page.view).element(page.button.submit)).click().perform();
    browser.waitForAngular();
    // show popup error
    browser.wait(element(page.popup.container).isDisplayed(), 5000, 'wait for popup');
    expect(element(page.popup.searchError).isPresent()).toBeTruthy();
    // click close
    browser.wait(element(page.popup.close).isPresent(), 5000, 'wait for close');
    browser.actions().mouseMove(element(page.popup.close)).click().perform();
  });
});
