var page = require('../../../uat-positive/profile/change-password/page');

describe(':: Change Password ::', function todo() {
  beforeAll(function before() {
    browser.setLocation(page.route);
  });

  it('should can\'t change password', function be() {
    // input field password
    element(page.view).element(page.input.password).clear().sendKeys('TEST');
    // input field password confirmation
    element(page.view).element(page.input.passwordConfirmation).clear().sendKeys('TSET');
    // show invalid input
    expect(element(page.view).element(page.button.submit).isEnabled()).toBeFalsy();
  });
});
