'use strict';

var autoprefixer = require('gulp-autoprefixer');
var bowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var del = require('del');
var eslint = require('gulp-eslint');
var gulp = require('gulp');
var inject = require('gulp-inject');
var minifyCss = require('gulp-clean-css');
var ngAnnotate = require('gulp-ng-annotate');
var ngDocs = require('gulp-ngdocs');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var styleLint = require('gulp-stylelint');
var uglify = require('gulp-uglify');

var cssFilesToInject = [
  'www/css/**/*.css'
];
var cssMinFilesToInject = [
  'www/css/**/*.min.css'
];
var jsFilesToInject = [
  'www/lib/ionic.js',
  'www/lib/angular.js',
  'www/lib/raven.js',
  'www/lib/ng-raven.js',
  'www/lib/**/*.js',
  'www/app/module.js',
  'www/app/**/module.js',
  '!www/app/**/controller.js',
  '!www/app/**/*test.js',
  'www/app/**/*.js'
];
var jsMinFilesToInject = [
  'www/js/**/*.min.js'
];

gulp.task('compile', function onBuild(done) {
  runSequence(
    'clean',
    ['bower', 'copy:plugins', 'copy:fonts', 'sass'],
    'inject', 'watch', done
  );
});

gulp.task('compile:prod', function onBuildProd(done) {
  runSequence(
    'clean', 'stylelint:prod', 'eslint:prod', 'bower', 'copy:plugins',
    'copy:fonts', 'sass:prod', 'js:prod', 'inject:prod',
    'ngdocs', done
  );
});

gulp.task('bower', function onBower(done) {
  gulp.src(bowerFiles())
    .pipe(gulp.dest('www/lib/'))
    .on('end', done);
});

gulp.task('clean', function onClean(done) {
  del.sync(['docs', 'www/css', 'www/fonts', 'www/js', 'www/lib']);
  done();
});

gulp.task('copy:fonts', function onCopyFonts(done) {
  runSequence([
    'copy:fonts-awesome',
    'copy:fonts-ionicons',
    'copy:fonts-sans'
  ], done);
});

gulp.task('copy:plugins', function onCopyPlugins(done) {
  runSequence([
    'copy:ravenjs'
  ], done);
});

gulp.task('copy:fonts-awesome', function onCopyFontsIonicons(done) {
  gulp.src('bower_components/font-awesome/fonts/**/*')
    .pipe(gulp.dest('www/fonts/font-awesome'))
    .on('end', done);
});

gulp.task('copy:fonts-ionicons', function onCopyFontsIonicons(done) {
  gulp.src('bower_components/ionic/release/fonts/**/*')
    .pipe(gulp.dest('www/fonts/ionicons'))
    .on('end', done);
});

gulp.task('copy:fonts-sans', function onCopyFontsIonicons(done) {
  gulp.src('bower_components/open-sans-fontface/fonts/**/*')
    .pipe(gulp.dest('www/fonts/sans'))
    .on('end', done);
});

gulp.task('copy:ravenjs', function onCopyPlugin(done) {
  gulp.src('bower_components/raven-js/dist/plugins/angular.js')
    .pipe(rename('ng-raven.js'))
    .pipe(gulp.dest('www/lib/'))
    .on('end', done);
});

gulp.task('eslint', function onEslint() {
  return gulp.src('www/app/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('eslint:prod', function onEslint() {
  return gulp.src('www/app/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .on('error', function onError() {
      process.exit(1);
    });
});

gulp.task('inject', ['eslint'], function onInject(done) {
  gulp.src('./www/main.html')
    .pipe(inject(gulp.src(cssFilesToInject, { read: false }), { relative: true, name: 'style', empty: true }))
    .pipe(inject(gulp.src(jsFilesToInject, { read: false }), { relative: true, name: 'script', empty: true }))
    .pipe(gulp.dest('www'))
    .on('end', done);
});

gulp.task('inject:prod', function onInjectProd(done) {
  gulp.src('./www/main.html')
    .pipe(inject(gulp.src(cssMinFilesToInject, { read: false }), { relative: true, name: 'style', empty: true }))
    .pipe(inject(gulp.src(jsMinFilesToInject, { read: false }), { relative: true, name: 'script', empty: true }))
    .pipe(gulp.dest('www'))
    .on('end', done);
});

gulp.task('js:prod', function onJsProd(done) {
  gulp.src(jsFilesToInject)
    .pipe(concat('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('www/js/'))
    .on('end', done);
});

gulp.task('ngdocs', function onNgdocs(done) {
  gulp.src('www/app/**/*.js')
    .pipe(ngDocs.process({ html5Mode: false }))
    .pipe(gulp.dest('docs'))
    .on('end', done);
});

gulp.task('sass', ['stylelint'], function onSass(done) {
  gulp.src('scss/app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
    .pipe(gulp.dest('www/css/'))
    .on('end', done);
});

gulp.task('sass:prod', function onSassProd(done) {
  gulp.src('scss/app.scss')
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
    .pipe(minifyCss({ specialComments: 0, processImport: 0 }))
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest('www/css/'))
    .on('end', done);
});

gulp.task('stylelint', function onStylelint() {
  return gulp.src('scss/**/*.scss')
    .pipe(styleLint({
      failAfterError: false,
      reporters: [
        { formatter: 'string', console: true }
      ]
    }));
});

gulp.task('stylelint:prod', function onStylelint() {
  return gulp.src('scss/**/*.scss')
    .pipe(styleLint({
      failAfterError: true,
      reporters: [
        { formatter: 'string', console: true }
      ]
    }))
    .on('error', function onError() {
      process.exit(1);
    });
});

gulp.task('watch', function onWatch() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(jsFilesToInject, ['inject'])
    .on('error', function onError(error) {
      if (/enoent/i.test(error.code));
    });
});

gulp.task('watch:docs', function onWatchDocs() {
  gulp.watch(['www/app/**/*.js'], ['clean', 'ngdocs']);
});

gulp.task('default', ['compile']);
gulp.task('build:before', ['compile:prod']);
gulp.task('run:before', ['compile']);
gulp.task('serve:before', ['compile']);
gulp.task('docs', ['ngdocs', 'watch:docs']);
