'use strict';

var express = require('express');
var path = require('path');
var pkg = require('./package');

var app = express();
var port = '4323';

app.use('/docs', express.static(path.resolve(__dirname, 'docs')));
app.use('/', express.static(path.resolve(__dirname, 'platforms', 'browser', 'www')));
app.listen(port, function on() {
  console.log('> ' + pkg.name + '@' + pkg.version + ' running on localhost:' + port); // eslint-disable-line
});
