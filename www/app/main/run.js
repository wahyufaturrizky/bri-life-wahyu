(function _AppMainRun_() {
  'use strict';

  function run(
    $ionicHistory, $q, $rootScope, $state, $window, $timeout,
    EVENT, analytic, auth, member, provider, dependent, onesignal
  ) {
    var $this = $rootScope;
    var widthPage = $window.innerWidth;
    var tags = {};

    $this.$root = {};
    $this.$root.logout = logout;
    $this.$root.widthPage = widthPage;
    $this.$on('$stateChangeStart', stateChangeStart);
    $this.$on(EVENT.LOGIN.SUCCESS, loginSuccess);
    $this.$on(EVENT.LOGOUT.SUCCESS, logoutSuccess);

    function loginSuccess() {
      analytic.addSession();
      member.initData();
      // provider.initData();
      provider.initCityAndPlans();
      $timeout(initOneSignal(), 12345);
    }

    function logoutSuccess() {
      member.clearData(member.key);
      dependent.remove();
      provider.clearData(provider.keys.cityPlans);
      provider.clearData(provider.keys.providers);
      provider.clearData(provider.keys.selectedCity);
      provider.clearData(provider.keys.selectedPlan);
      $timeout(onesignal.deleteTags(_.keys(tags)), 1234);
    }

    function initOneSignal() {
      $q.resolve()
        .then(function resolve() {
          return member.getData();
        })
        .then(function resolve(data) {
          if (!(data && data.profile)) return {};
          return data.profile;
        })
        .then(function resolve(data) {
          return {
            id: data.id,
            email: data.email,
            name: data.name,
            cardNumber: data.admedika && data.admedika.cardNumber,
            payorInfo: data.admedika && data.admedika.payorInfo,
            corporateInfo: data.admedika && data.admedika.corporateInfo
          };
        })
        .then(function resolve(data) {
          tags = data;
          return onesignal.sendTags(tags);
        });
    }

    function stateChangeStart(event, toState, toParams, fromState) {
      var screen = $window.screen;
      var Platform = $window.ionic && $window.ionic.Platform;

      if (toState.name === 'app.main.admedika.show') {
        if (screen && screen.lockOrientation) {
          screen.lockOrientation('landscape');
        }
        if (Platform && Platform.fullScreen) {
          Platform.fullScreen(true, false);
        }
      }

      if (fromState.name === 'app.main.admedika.show') {
        if (screen && screen.unlockOrientation) {
          screen.lockOrientation('portrait');
        }
        if (Platform && Platform.fullScreen) {
          Platform.fullScreen(false, true);
        }
      }

      if (toState.name === 'app.main.home') {
        clearHistory();
      }
    }

    function clearHistory() {
      $ionicHistory.nextViewOptions({
        disableBack: true,
        historyRoot: true
      });
    }

    function logout() {
      $q.resolve(auth.logout())
        .then(function resolve() {
          clearHistory();
          $state.go('app.auth.login');
        });
    }
  }

  angular.module('app.main').run(run);
}());
