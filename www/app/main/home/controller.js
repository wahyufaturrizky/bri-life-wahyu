(function _AppMainHomeController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.home.controller:MainHomeController
   *
   * @requires $scope
   * @requires $state
   * @requires $timeout
   * @requires app.providers.graphql.service:graphql
   * @requires app.providers.toast.factory:toast
   *
   * @description
   * Home controller.
   */
  function MainHomeController(
    $ionicSlideBoxDelegate, $cordovaInAppBrowser, $scope, $state,
    $window, $timeout, graphql, member, toast, myadmedika
  ) {
    var vm = this;
    var location = $window.location;

    vm.interval = 4321;
    vm.getMember = getMember;
    vm.hasMember = false;
    vm.profile = {};
    vm.images = [];
    vm.menus = [];
    vm.slideChanged = slideChanged;
    vm.open = open;
    vm.openUrl = openUrl;

    $timeout(getImages);
    $timeout(getMenus);

    function open(img) {
      var admedika = angular.copy(vm.profile.admedika);
      if (!vm.hasMember) return toast.shortBottom('Please wait...');
      if (admedika && admedika.cardNumber) {
        if (myadmedika.isActive(admedika.member) &&
           !myadmedika.isExpired(admedika.plans) &&
           !myadmedika.isLost(admedika.member)) {
          if (img && img.uri) {
            if (img.isExternal) {
              try {
                $cordovaInAppBrowser.open(img.uri, '_system', { zoom: 'no' });
              } catch (e) {
                location.href = img.uri;
              }
            } else $state.go(img.uri);
          }
        } else $state.go('app.main.admedika.reconnect', { cardNumber: admedika.cardNumber });
      } else $state.go('app.main.admedika.reconnect');
      return false;
    }

    function slideChanged(index) {
      if (($ionicSlideBoxDelegate.slidesCount() - 1) === index) {
        $timeout(function o() {
          $ionicSlideBoxDelegate.slide(0);
        }, vm.interval);
      }
    }

    function getImages() {
      var query = [
        'query Sliders($match: String, $limit: Int, $sort: String) {',
        '  sliders(match: $match, limit: $limit, sort: $sort) {',
        '    _id',
        '    index',
        '    image',
        '    uri',
        '    isExternal',
        '  }',
        '}'
      ].join('');
      var variable = {
        sort: 'index',
        limit: Infinity,
        match: angular.toJson({ page: { $regex: 'home', $options: 'i' } })
      };

      graphql(query, variable)
        .then(function resolve(res) {
          return angular.copy(res);
        })
        .then(function resolve(res) {
          vm.images = res.sliders;
          $timeout(function o() {
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.start();
            $scope.$apply();
          });
        });
    }

    function getMenus() {
      var query = [
        'query Sliders($match: String, $limit: Int, $sort: String) {',
        '  sliders(match: $match, limit: $limit, sort: $sort) {',
        '    _id',
        '    index',
        '    image',
        '    uri',
        '    isExternal',
        '  }',
        '}'
      ].join('');
      var variable = {
        sort: 'index',
        limit: Infinity,
        match: angular.toJson({ page: { $regex: 'menu', $options: 'i' } })
      };

      graphql(query, variable)
        .then(function resolve(res) {
          return angular.copy(res);
        })
        .then(function resolve(res) {
          vm.menus = res.sliders;
          $timeout(function o() {
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.start();
            $scope.$apply();
          });
        });
    }

    /**
     * @ngdoc function
     * @name app.main.home.controller#getMember
     * @methodOf app.main.home.controller:MainHomeController
     *
     * @description
     * Get user profile.
     */
    function getMember() {
      member.getData().then(function resolve(res) {
        if (!(res && res.profile) && !(member.hasData)) {
          $timeout(getMember, 1234);
        } else {
          vm.hasMember = true;
          angular.copy(res.profile, vm.profile);
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      });
    }

    function openUrl() {
      var url = 'http://www.bringinlife.co.id/';
      try {
        $cordovaInAppBrowser.open(url, '_system', { zoom: 'no' });
      } catch (e) {
        location.href = url;
      }
    }
  }

  angular.module('app.main.home').controller('MainHomeController', MainHomeController);
}());
