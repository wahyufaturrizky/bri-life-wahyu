(function _AppMainSettingController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.setting.controller:MainSettingController
   *
   * @requires $cordovaAppVersion
   * @requires $q
   * @requires $window
   * @requires app.providers.graphql.service:graphql
   *
   * @description
   * Setting controller.
   */
  function MainSettingController($cordovaAppVersion, $q, $window, graphql) {
    var vm = this;

    vm.app = {};
    vm.support = undefined;

    getConfig();

    $window.document.addEventListener('deviceready', function ready() {
      $q.all([
        $cordovaAppVersion.getAppName(),
        $cordovaAppVersion.getVersionNumber()
      ])
      .then(function resolve(res) {
        angular.merge(vm.app, { name: res[0], version: res[1] });
      });
    }, false);

    function getConfig() {
      var query = [
        'query AppsConfig {',
        '  config {',
        '    apps {',
        '      support',
        '    }',
        '  }',
        '}'
      ].join('');

      graphql(query)
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.config && result.config.apps;
        })
        .then(function resolve(res) {
          vm.support = res.support;
        })
        .catch(function reject() {
          //
        });
    }
  }

  angular.module('app.main.setting').controller('MainSettingController', MainSettingController);
}());
