(function _AppMainChangeBPJSRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.main.profile.change-bpjs', {
      url: '/change-bpjs',
      views: {
        'menuContent@app.main.profile': {
          templateUrl: 'app/main/profile/change-bpjs/index.html',
          controller: 'MainProfileMineController',
          controllerAs: '$'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/main/profile/mine/controller.js');
        }
      }
    });
  }

  angular.module('app.main.profile.change-bpjs').config(route);
}());
