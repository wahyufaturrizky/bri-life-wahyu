(function _AppMainChangeBPJSModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.main.profile.change-bpjs
   * @description Change BPJS module
   * ### Route
   * `/profile/change-BPJS`
   */

  /**
   * @ngdoc overview
   * @name app.main.profile.change-bpjs
   *
   * @description
   * Change BPJS module.
   *
   * ### Route
   * - `/profile/change-BPJS`
   *
   * ### Controller
   * - {@link app.main.profile.mine.controller:MainProfileMineController}
   */
  angular.module('app.main.profile.change-bpjs', modules);
}());
