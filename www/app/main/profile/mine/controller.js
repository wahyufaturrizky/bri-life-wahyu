(function _AppMainProfileMineController_() {
  'use strict';

   /**
    * @ngdoc controller
    * @name app.main.profile.mine.controller:MainProfileMineController
    *
    * @requires $rootScope
    * @requires $scope
    * @requires $ionicHistory
    * @requires $state
    * @requires $q
    * @requires app.providers.toast.factory:toast
    * @requires app.providers.graphql.service:graphql
    * @requires EVENT
    *
    * @description
    * My profile controller.
    */
  function MainProfileMineController(
    $rootScope, $scope, $ionicHistory, $state, $q, toast, graphql, EVENT) {
    var $this = $rootScope;
    var vm = this;

    vm.getProfile = getProfile;
    vm.hasProfile = false;
    vm.parseBirthday = parseBirthday;
    vm.profile = {};
    vm.isLoading = false;
    vm.submit = submit;

    $this.$on(EVENT.PHOTO.CHANGED, function changed(event, args) {
      $scope.$evalAsync(function sync() {
        angular.merge(vm.profile, { photo: args });
      });
    });

    /**
     * @ngdoc function
     * @name app.main.profile.mine.controller#getProfile
     * @methodOf app.main.profile.mine.controller:MainProfileMineController
     *
     * @description
     * Get profile of the user.
     */
    function getProfile() {
      var data = {};
      var query = [
        'query Profile {',
        '  profile {',
        '    _id',
        '    birthday',
        '    name',
        '    email',
        '    phone',
        '    id',
        '    photo',
        '    admedika {',
        '      _id',
        '      cardNumber',
        '      bpjsInfo {',
        '        _id',
        '        bpjsId',
        '        faskesLevel1',
        '        bpjsType',
        '      }',
        '      member {',
        '        memberType',
        '        bpjsId',
        '        faskesLevel1',
        '        bpjsType',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      graphql(query)
        .then(function resolve(res) {
          angular.copy(res.profile, vm.profile);
          console.log(vm.profile); //eslint-disable-line
          if (!vm.profile.admedika.bpjsInfo._id) {
            Object.assign(data, {
              bpjsId: vm.profile.admedika.member.bpjsId,
              faskesLevel1: vm.profile.admedika.member.faskesLevel1,
              bpjsType: vm.profile.admedika.member.bpjsType
            });
            submit(data);
          }
        })
        .finally(function end() {
          vm.hasProfile = true;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    }

    /**
     * @ngdoc function
     * @name app.main.profile.mine.controller#parseBirthdate
     * @methodOf app.main.profile.mine.controller:MainProfileMineController
     *
     * @description
     * Parse birthdate.
     *
     * @param {string} date Date of birth.
     *
     * @returns {string} formatted birthdate.
     */
    function parseBirthday(date) {
      if (date && moment(date).isValid()) {
        return moment(date).format('DD MMM YYYY');
      }
      return date;
    }

      /**
     * @ngdoc function
     * @name app.main.profile.mine.controller#submit
     * @methodOf app.main.profile.mine.controller:MainProfileMineController
     *
     * @description
     * Processing change BPJS.
     *  If success, user will be redirected to profile page.
     *  Otherwise, an error toast will be shown.
     *
     * @param {Object} variables Object describing the user BPJS.
     *  The object has following properties:
     *  - **ADMEDIKA ID** - `{string}` - ID of Admedika.
     *  - **BPJS ID**   - `{string}` - BPJS ID of the user.
     *  - **FASKES LEVEL 1** - `{string}` - FASKES LEVEL1 of the user.
     *  - **BPJS TYPE** - `{string}` - BPJS TYPE the user.
     */
    function submit(variables) {
      var query;
      var queryCreate = [
        'mutation createBPJS($admedikaId: ID!, $bpjsId: String!, $faskesLevel1: String!, $bpjsType: String!) {',
        '  createBPJS(admedikaId: $admedikaId, bpjsId: $bpjsId, faskesLevel1: $faskesLevel1, bpjsType: $bpjsType){_id}',
        '}'
      ].join('');

      var queryEdit = [
        'mutation editBPJS($_id: ID!, $bpjsId: String!, $faskesLevel1: String!, $bpjsType: String!) {',
        '  editBPJS(_id: $_id, bpjsId: $bpjsId, faskesLevel1: $faskesLevel1, bpjsType: $bpjsType){_id}',
        '}'
      ].join('');

      Object.assign(variables, { admedikaId: vm.profile.admedika._id });
      if (!(variables.admedikaId.bpjsInfo)) {
        query = queryCreate;
      } else {
        query = queryEdit;
        Object.assign(variables, { _id: vm.profile.admedika.bpjsInfo._id });
      }

      $q.resolve(true)
        .then(function resolve() {
          if (!(variables.bpjsId.length > 12)) {
            throw new Error('BPJS ID minimal 13 karakter');
          }
          vm.isLoading = true;
          return graphql(query, variables);
        })
        .then(function resolve() {
          return $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: false,
            historyRoot: true
          });
        })
        .then(function resolve() {
          $state.go('app.main.admedika.member');
          toast.longBottom('BPJS berhasil diubah.');
        })
        .catch(function reject(err) {
          toast.shortBottom(err.toString().replace('Error: ', ''));
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }
  }

  angular.module('app.main.profile').controller('MainProfileMineController', MainProfileMineController);
}());
