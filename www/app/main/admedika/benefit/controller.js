(function _AppMainAdmedikaBenefitController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.admedika.benefit.controller:MainAdmedikaBenefitController
   *
   * @requires $scope
   * @requires app.providers.graphql.service:graphql
   * @requires app.providers.toast.factory:toast
   *
   * @description
   * AdMedika benefit controller.
   */
  function MainAdmedikaBenefitController($filter, $scope, graphql, toast, dependent, $q) {
    var vm = this;
    var plan = {};
    var config = {};

    vm.getPlans = getPlans;
    vm.hasPlans = false;
    vm.hasConfig = false;
    vm.isGroupShown = isGroupShown;
    vm.isShown = isShown;
    vm.parseDate = parseDate;
    vm.parseFrequencyDesc = parseFrequencyDesc;
    vm.parseMaxIdr = parseMaxIdr;
    vm.plans = [];
    vm.shownGroup = null;
    vm.toggleGroup = toggleGroup;
    vm.profile = {};
    vm.isPrudential = isPrudential;

    plan.skip = 0;
    plan.limit = 10;
    plan.sort = 'planType';

    function getConfig() {
      var payor;
      var corporate;
      $q.resolve(true)
        .then(function resolve() {
          if (config && config.payor) return config;
          if (vm.profile && vm.profile.admedika && vm.profile.admedika.member) {
            payor = vm.profile.admedika.member.payorInfo;
            corporate = vm.profile.admedika.member.corporateInfo;
            if (!(payor && corporate)) return {};
            return getBenefitView({ payor: payor, corporate: corporate });
          }
          return {};
        })
        .then(function resolve(res) {
          if (res && res.payor) return res;
          return getBenefitView({ payor: 'default', corporate: 'default' });
        })
        .then(function resolve(res) {
          return angular.copy(res);
        })
        .then(function resolve(res) {
          angular.copy(res, config);
        })
        .catch(function reject() {
          //
        })
        .finally(function end() {
          vm.hasConfig = true;
        });
    }

    function getBenefitView(match) {
      var query = [
        'query BenefitViews($limit: Int, $skip: Int, $sort: String, $match: String) {',
        '  benefitViews(limit: $limit, skip: $skip, sort: $sort, match: $match) {',
        '    payor',
        '    corporate',
        '    planType',
        '    planPolicyStartDate',
        '    planPolicyEndDate',
        '    planMaxIDR',
        '    planFrequencyDescription',
        '    planCurrentLimit',
        '    benefitName',
        '    benefitMaxIDR',
        '    benefitAvailableLimit',
        '    benefitFrequencyDescription',
        '  }',
        '}'
      ].join('');

      return $q.resolve(true)
        .then(function resolve() {
          return graphql(query, {
            limit: 1,
            skip: 0,
            sort: 'createdAt',
            match: angular.toJson(match)
          });
        })
        .then(function resolve(res) {
          return angular.copy(res && res.benefitViews);
        })
        .then(function resolve(res) {
          return res && res.length ? res.shift() : {};
        })
        .catch(function reject() {
          //
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#getBenefits
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Get benefits of the member based on selected plan.
     *
     * @param {string} planId ID of the plan.
     */
    function getBenefits(index, planId) {
      // var index = _.findIndex(vm.plans, { planId: planId });
      var item = vm.plans[index];
      var query = [
        'query Benefit($cardNo: String, $birthDate: String, $planId: String) {',
        '  profile {',
        '    admedika(cardNumber: $cardNo, birthday: $birthDate) {',
        '      benefits(planId: $planId) {',
        '        benefitName',
        '        maxIdr',
        '        frequencyDesc',
        '        available',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      if (item && item.benefits && item.benefits.length) return;

      $q.resolve()
        .then(function resolve() {
          return dependent.get();
        })
        .then(function resolve(variables) {
          return graphql(query, angular.merge({ planId: planId }, variables));
        })
        .then(function resolve(res) {
          var data = angular.copy(res);
          return data && data.profile && data.profile.admedika && data.profile.admedika.benefits;
        })
        .then(function resolve(res) {
          return res && res.length ? res : [];
        })
        .then(function resolve(res) {
          return _.filter(res, 'benefitName');
        })
        .then(function resolve(res) {
          angular.merge(vm.plans[index], { benefits: res, hasBenefits: true });
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#getPlans
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Get plans of the member.
     */
    function getPlans() {
      var query = [
        'query Plan($cardNo: String, $birthDate: String, $skip: Int, $limit: Int, $sort: String) {',
        '  profile {',
        '    admedika(cardNumber: $cardNo, birthday: $birthDate) {',
        '      member {',
        '        payorInfo',
        '        corporateInfo',
        '      }',
        '      plans(skip: $skip, limit: $limit, sort: $sort) {',
        '        planId',
        '        planType',
        '        planStatus',
        '        policyStartDate',
        '        policyEndDate',
        '        recordStatus',
        '        maxIdr',
        '        frequencyDesc',
        '        currentLimit',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      $q.resolve()
        .then(function resolve() {
          return dependent.get();
        })
        .then(function resolve(variables) {
          return graphql(query, angular.merge(plan, variables));
        })
        .then(function resolve(res) {
          var data = angular.copy(res);
          angular.copy(data.profile, vm.profile);
          return data && data.profile && data.profile.admedika && data.profile.admedika.plans;
        })
        .then(function resolve(res) {
          return res && res.length ? res : [];
        })
        .then(function resolve(res) {
          vm.plans = vm.plans.concat(_.filter(_.reject(res, function c(o) {
            return /suspend/i.test(o.planStatus);
          }), 'planId'));
          vm.hasPlans = plan.limit > res.length;

          plan.skip += plan.limit;
        })
        .then(function resolve() {
          return getConfig();
        })
        .catch(function reject() {
          toast.shortBottom('Failed to get benefit info');
        })
        .finally(function done() {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#isGroupShown
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Check wheter plan is selected or not.
     *
     * @param {integer} index Index of plan.
     *
     * @returns {Boolean} status of selected plan.
     */
    function isGroupShown(index) {
      return vm.shownGroup === index;
    }

    function isPrudential(payorInfo) {
      return /^pla$/i.test(payorInfo);
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#parseDate
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Replace `-` with `/` on benefit date.
     *
     * @param {string} date Date of benefit.
     *
     * @returns {string} formatted date.
     */
    function parseDate(date) {
      return date.replace(/-/g, '/');
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#parseMaxIdr
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Replace `999.999.999` with `As Charged`.
     *
     * @param {string} maxIdr MaxIdr of benefit.
     *
     * @returns {string} formatted maxIdr.
     */
    function parseMaxIdr(maxIdr) {
      if (maxIdr.split('').every(function c(o) {
        return o === '.' || o === '9' || o === 9;
      })) return 'As Charged';
      return $filter('number')(maxIdr, 0);
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#parseFrequencyDesc
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Replace (existing) and append `per` on frequency description benefit.
     *
     * @param {string} desc Frequency description of benefit.
     *
     * @returns {string} formatted frequency description.
     */
    function parseFrequencyDesc(desc) {
      return 'per '.concat(desc.replace(/per\s+/gi, ''));
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.benefit.controller#toggleGroup
     * @methodOf app.main.admedika.benefit.controller:MainAdmedikaBenefitController
     *
     * @description
     * Toggle group of benefits based on selected plan.
     *
     * @param {string} planId ID of the plan.
     */
    function toggleGroup(index, planId) {
      vm.shownGroup = vm.isGroupShown(index) ? null : index;
      if (!(vm.shownGroup === null)) getBenefits(index, planId);
    }

    function isShown(field) {
      if (!(config && config.payor)) return true;
      return config[field];
    }
  }

  angular.module('app.main.admedika.benefit').controller('MainAdmedikaBenefitController', MainAdmedikaBenefitController);
}());
