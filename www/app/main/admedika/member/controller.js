(function _AppMainAdmedikaMemberController_() {
  'use strict';

   /**
    * @ngdoc controller
    * @name app.main.admedika.member.controller:MainAdmedikaMemberController
    *
    * @requires $ionicHistory
    * @requires $rootScope
    * @requires $scope
    * @requires $timeout
    * @requires app.providers.graphql.service:graphql
    * @requires app.providers.toast.factory:toast
    * @requires EVENT
    *
    * @description
    * AdMedika member controller.
    */

  function MainAdmedikaMemberController(
    $ionicHistory, $ionicPopup, $rootScope, $scope, graphql, toast, EVENT,
    $ionicPlatform, $state, member, dependent, $q
  ) {
    var $root = $rootScope;
    var vm = this;
    var deregisterBackButtonAction = $ionicPlatform.registerBackButtonAction(customBackButton, 101);

    function customBackButton() {
      switch ($state.current.name) {
        case 'app.main.admedika.member':
          $state.go('app.main.home');
          break;
        case 'app.main.admedika.connect':
        case 'app.main.admedika.benefit':
        case 'app.main.admedika.claim':
        case 'app.main.admedika.provider':
          $state.go('app.main.admedika.member');
          break;
        default:
          // console.log($state.current.name);
          $ionicHistory.goBack();
          // toast.shortBottom($state.current.name);
      }
    }

    function customSoftBack() {
      customBackButton();
    }

    function destroyCustomHardBackButton() {
      deregisterBackButtonAction();
    }

    // customBackButton = customBackButton.bind(null, $state)

    // override default behaviour
    $root.$ionicGoBack = customSoftBack;

    // Effectively making the callback applied in this particular controller
    $scope.$on('$destroy', destroyCustomHardBackButton);

    vm.back = $ionicHistory.goBack;
    vm.getMember = getMember;
    vm.hasMember = false;
    vm.parseBenefits = parseBenefits;
    vm.parseBirthdate = parseBirthdate;
    vm.showDependents = showDependents;
    vm.profile = {};

    $root.$on(EVENT.PHOTO.CHANGED, function changed(event, args) {
      $scope.$evalAsync(function sync() {
        angular.merge(vm.profile, { photo: args });
      });
    });

    /**
     * @ngdoc function
     * @name app.main.admedika.member.controller#getMember
     * @methodOf app.main.admedika.member.controller:MainAdmedikaMemberController
     *
     * @description
     * Get member details of the user.
     */
    function getMember() {
      var query = [
        'query Member($cardNo: String, $birthDate: String) {',
        '  profile {',
        '    _id',
        '    email',
        '    birthday',
        '    id',
        '    photo',
        '    admedika(cardNumber: $cardNo, birthday: $birthDate) {',
        '      cardNumber',
        '      plans {',
        '        planType',
        '      }',
        '      bpjsInfo {',
        '        bpjsId',
        '        faskesLevel1',
        '        bpjsType',
        '      }',
        '      member {',
        '        fullName',
        '        payorInfo',
        '        corporateInfo',
        '        policyNumber',
        '        dateOfBirth',
        '        memberId',
        '        memberType',
        '        planType',
        '        helpLine',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      $q.resolve()
        .then(function resolve() {
          return dependent.get();
        })
        .then(function resolve(variables) {
          return graphql(query, variables);
        })
        .then(function resolve(res) {
          angular.copy(res.profile, vm.profile);
        })
        .catch(function reject() {
          toast.shortBottom('Failed to get member info');
        })
        .finally(function done() {
          vm.hasMember = true;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.member.controller#parseBenefits
     * @methodOf app.main.admedika.member.controller:MainAdmedikaMemberController
     *
     * @description
     * Concat benefit with `,` separator.
     *
     * @param {Array} benefits Array of benefits.
     *
     * @returns {string} formatted benefits.
     */
    function parseBenefits(benefits) {
      var data = [];
      if (!(benefits && benefits.length)) return '-';
      benefits.forEach(function onEach(benefit) {
        if (Object.prototype.hasOwnProperty.call(benefit, 'planType') && benefit.planType) {
          if (data.indexOf(benefit.planType) < 0) data.push(benefit.planType);
        }
      });
      return data.filter(Boolean).join(', ');
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.member.controller#parseBirthdate
     * @methodOf app.main.admedika.member.controller:MainAdmedikaMemberController
     *
     * @description
     * Parse birthdate.
     *
     * @param {string} date Date of birth.
     *
     * @returns {string} formatted birthdate.
     */
    function parseBirthdate(date) {
      if (date && moment(date).isValid()) {
        return moment(date).format('DD MMM YYYY');
      }
      return date;
    }

    function showDependents() {
      member.getData()
        .then(function resolve(res) {
          return res && res.profile && res.profile.admedika;
        })
        .then(function resolve(res) {
          var scope = $scope;
          var current = vm.profile.admedika.member;

          if (!(res && res.dependents)) throw new Error('Failed to get dependents');
          if (!(res && res.member && res.member.memberType)) throw new Error('Failed to get member type');
          if (!/principal|principle/i.test(res.member.memberType)) return showDependentsFailed();

          scope.data = {};
          scope.data.member = res.member;
          scope.data.dependent = res.member;
          scope.data.dependents = res.dependents.filter(function c(v) {
            return v.fullName;
          }).map(function c(v) {
            if (angular.equals(v.fullName, current.fullName)) {
              scope.data.dependent = v;
            }
            return v;
          });

          return $ionicPopup.show({
            scope: scope,
            templateUrl: 'app/main/admedika/member/modal/switch-dependent.html',
            cssClass: 'switch-account-popup',
            buttons: [
              { text: 'Cancel' },
              {
                text: 'Switch',
                type: 'button-default',
                onTap: function onTap() {
                  if (!angular.equals(scope.data.dependent.fullName, current.fullName)) {
                    if (angular.equals(scope.data.dependent.fullName, res.member.fullName)) {
                      dependent.remove();
                    } else {
                      dependent.set(scope.data.dependent);
                    }
                    vm.profile = {};
                    vm.hasMember = false;
                    getMember();
                  }
                }
              }
            ]
          });
        })
        .catch(function reject(error) {
          toast.shortBottom(error);
        });
    }

    function showDependentsFailed() {
      $ionicPopup.alert({
        templateUrl: 'app/main/admedika/member/modal/switch-error.html',
        cssClass: 'mp-popup-style',
        okText: 'Tutup',
        okType: 'button-default'
      });
    }
  }

  angular.module('app.main.admedika.member').controller('MainAdmedikaMemberController', MainAdmedikaMemberController);
}());
