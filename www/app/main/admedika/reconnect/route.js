(function _AppMainAdmedikaReconnectRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.main.admedika.reconnect', {
      url: '/reconnect?cardNumber',
      views: {
        'menuContent@app.main.admedika': {
          templateUrl: 'app/main/admedika/reconnect/index.html',
          controller: 'MainAdmedikaReconnectController',
          controllerAs: '$'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/main/admedika/reconnect/controller.js');
        }
      }
    });
  }

  angular.module('app.main.admedika.reconnect').config(route);
}());
