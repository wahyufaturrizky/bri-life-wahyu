(function _AppMainAdmedikaReconnectModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.main.admedika.reconnect
   *
   * @description
   * AdMedika reconnect module.
   *
   * ### Route
   * - `/admedika/reconnect`
   *
   * ### Controller
   * - {@link app.main.admedika.reconnect.controller:MainAdmedikaReconnectController}
   */
  angular.module('app.main.admedika.reconnect', modules);
}());
