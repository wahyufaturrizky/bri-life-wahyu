(function _AppMainAdmedikaReconnectController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.admedika.reconnect.controller:MainAdmedikaReconnectController
   *
   * @requires $ionicPopup
   * @requires app.providers.graphql.service:graphql
   *
   * @description
   * AdMedika reconnect controller.
   */
  function MainAdmedikaReconnectController($ionicPopup, $q, $rootScope, graphql, $stateParams) {
    var vm = this;
    var $this = $rootScope;

    vm.cardNumber = $stateParams.cardNumber;
    vm.failed = false;
    vm.isLoading = false;
    vm.submit = submit;

    /**
     * @ngdoc function
     * @name app.main.admedika.reconnect.controller#submit
     * @methodOf app.main.admedika.reconnect.controller:MainAdmedikaReconnectController
     *
     * @description
     * Processing connect admedika card number.
     *  If success, a success popup will be shown.
     *  Otherwise, an error popup will be shown.
     *
     * @param {Object} variables Object describing the member credentials.
     *  The object has following properties:
     *  - **cardNumber** - `{string}` - AdMedika card number of the member.
     */
    function submit(variables) {
      var query = [
        'mutation ConnectAdmedika($cardNumber: String!) {',
        '  connectAdmedika(cardNumber: $cardNumber)',
        '}'
      ].join('');
      var loading = $ionicPopup.show({
        templateUrl: 'app/main/admedika/reconnect/modal/loading.html',
        cssClass: 'reconnect-popup'
      });

      vm.isLoading = true;

      $q.resolve()
        .then(function resolve() {
          return graphql(query, variables);
        })
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.connectAdmedika;
        })
        .then(function resolve(res) {
          if (!(res)) throw new Error();
          loading.close();
          return $ionicPopup.alert({
            templateUrl: 'app/main/admedika/reconnect/modal/success.html',
            cssClass: 'reconnect-popup',
            okText: 'Log In',
            okType: 'button-default'
          });
        })
        .then(function resolve() {
          $this.$root.logout();
        })
        .catch(function reject() {
          vm.failed = true;
          loading.close();
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }
  }

  angular.module('app.main.admedika.reconnect').controller('MainAdmedikaReconnectController', MainAdmedikaReconnectController);
}());
