(function _AppMainAdmedikaProviderController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.admedika.provider.controller:MainAdmedikaProviderController
   *
   * @requires $ionicPopup
   * @requires $q
   * @requires $scope
   * @requires $timeout
   * @requires $window
   * @requires app.providers.geolocation.factory:geolocation
   * @requires app.providers.graphql.service:graphql
   * @requires app.providers.toast.factory:toast
   *
   * @description
   * AdMedika provider controller.
   */
  function MainAdmedikaProviderController(
    $ionicPopup, $q, $scope, $timeout, $window,
    geolocation, graphql, provider, toast
  ) {
    var vm = this;
    var param = {};
    var maps = {};

    vm.alertLoading = alertLoading;
    vm.getProviders = getProviders;
    vm.groupProviders = [];
    vm.hasProviders = false;
    vm.hasData = false;
    vm.nearMe = nearMe;
    vm.isLoading = false;
    vm.profile = {};
    vm.reset = reset;
    vm.search = search;
    vm.submit = submit;
    vm.sort = sort;
    vm.show = show;
    vm.parsePlans = parsePlans;
    vm.selectedCity = '';
    vm.selectedPlan = '';
    vm.hasSelectedCityAndPlan = false;
    vm.changeCity = changeCity;
    vm.changePlan = changePlan;
    vm.isReset = false;

    param.sort = 'providerName';
    param.skip = 0;
    param.limit = 10;
    param.data = [];
    param.providerCity = '';
    param.planId = '*';
    param.match = '';
    param.nearMe = '';

    getMapKey();
    getSelectedCityAndPlan();

    function parsePlans(plans) {
      return _.reject(plans || [], function c(o) {
        return /suspend/i.test(o.planStatus);
      });
    }

    function getSelectedCityAndPlan() {
      vm.isLoading = true;
      $q.resolve()
        .then(function resolve() {
          return getCityAndPlans();
        })
        .then(function resolve() {
          return $q.all([provider.getSelectedCity(), provider.getSelectedPlan()]);
        })
        .then(function resolve(res) {
          if (res && res.length) {
            if (res[0]) {
              vm.selectedCity = res[0];
              param.providerCity = vm.selectedCity;
            }
            if (res[1] && res[1] !== '*') {
              vm.selectedPlan = res[1];
              param.planId = vm.selectedPlan;
            }
            if (res[0] && res[1] && res[1] !== '*') {
              vm.hasSelectedCityAndPlan = true;
            }
          }
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#getCityAndPlans
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get cities and plans of the member.
     */
    function getCityAndPlans() {
      provider.getCityAndPlans().then(function resolve(res) {
        if (!(res && res.profile && res.areas && res.areas.length) && !(provider.hasCityPlans)) {
          $timeout(getCityAndPlans, 2345);
        } else {
          angular.merge(vm.profile, res.profile, { areas: res.areas });
        }
      });
    }

        /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#alertLoading
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Invoke alert on user actions (filter, sort, etc...) when the page still loading.
     */
    function alertBenefit() {
      $ionicPopup.alert({
        templateUrl: 'app/main/admedika/provider/modal/benefit-required.html',
        cssClass: 'mp-popup-style',
        okText: 'Tutup'
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#alertLoading
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Invoke alert on user actions (filter, sort, etc...) when the page still loading.
     */
    function alertLoading() {
      $ionicPopup.alert({
        templateUrl: 'app/main/admedika/provider/modal/loading.html',
        cssClass: 'mp-popup-style',
        okText: 'Tutup'
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#alertGpsError
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Invoke alert when gps or location of user not found.
     */
    function alertGpsError() {
      $ionicPopup.alert({
        templateUrl: 'app/main/admedika/provider/modal/gps-error.html',
        cssClass: 'mp-popup-style',
        okText: 'Tutup'
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#call
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Invoke phone dialer with filled phone number.
     *
     * @param {string} number Phone number to be dialed.
     */
    function call(number) {
      var location = $window.location;

      location.href = 'tel:'.concat(number);
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#getMapKey
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get google maps api key.
     */
    function getMapKey() {
      var query = [
        'query GoogleConfig {',
        '  config {',
        '    google {',
        '      geolocation {',
        '        apiKey',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      graphql(query)
        .then(function success(res) {
          angular.copy(res.config.google.geolocation, maps);
        })
        .catch(function reject() {
          getMapKey();
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#getProviders
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get providers of the member.
     */
    function getProviders() {
      var query = [
        'query Provider($planId: String, $providerCity: String, $match: String, $nearMe: String, $sort: String, $skip: Int, $limit: Int) {',
        '  profile {',
        '    admedika {',
        '      providers(planId: $planId, providerCity: $providerCity, match: $match, nearMe: $nearMe, sort: $sort, skip: $skip, limit: $limit) {',
        '        providerId',
        '        providerName',
        '        providerCity',
        '        providerAddress',
        '        providerPhoneNum',
        '        longitude',
        '        latitude',
        '        distance {',
        '          text',
        '          value',
        '        }',
        '        plans {',
        '          planId',
        '          planType',
        '          planStatus',
        '        }',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      $q.resolve(true)
        .then(function resolve() {
          vm.isLoading = true;
          return graphql(query, _.omit(param, ['sort', 'data']));
        })
        .then(function resolve(res) {
          return res && res.profile && res.profile.admedika && res.profile.admedika.providers;
        })
        .then(function resolve(res) {
          return res && res.length ? res : [];
        })
        .then(function resolve(res) {
          if (res.length && _.every(res, function c(o) {
            return _.some(param.data, function f(e) {
              return _.isEqual(o, e);
            });
          })) return;

          param.data = param.data.concat(_.filter(res, 'providerName'));
          param.skip += param.limit;

          vm.hasProviders = param.limit > res.length;
          vm.groupProviders = _.uniq(param.data, 'providerId');
          vm.isLoading = false;
          vm.hasData = true;
        })
        .finally(function done() {
          vm.isLoading = false;
          $timeout(function o() {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          });
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#nearMe
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get near providers of the member based on current location.
     */
    function nearMe(data) {
      if (data && data.planId === '*') {
        alertBenefit();
        return;
      }
      $q.resolve(true)
        .then(function resolve() {
          toast.longBottom('Getting location...');
          return geolocation.getPosition();
        })
        .then(function resolve(res) {
          if (data) {
            if (angular.isDefined(data.providerCity)) vm.selectedCity = data.providerCity;
            if (angular.isDefined(data.planId)) vm.selectedPlan = data.planId;
          }

          param.nearMe = [res.coords.latitude, res.coords.longitude].join(',');
          param.data = [];
          param.skip = 0;

          vm.groupProviders = [];
          vm.hasProviders = false;
          vm.latlong = param.nearMe;

          provider.setSelected(provider.keys.selectedCity, vm.selectedCity);
          provider.setSelected(provider.keys.selectedPlan, vm.selectedPlan);

          vm.hasSelectedCityAndPlan = true;

          angular.merge(param, data, { providerCity: vm.selectedCity, planId: vm.selectedPlan });

          $timeout(function o() {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          });
        })
        .catch(function reject(err) {
          if (/timeout/i.test(err)) alertGpsError();
          else toast.shortBottom(err);
        });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#reset
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Reset filtering providers data.
     *
     * @param {Object} variables Object describing the filtering data.
     */
    function reset(filter) {
      param.skip = 0;
      param.providerCity = '';
      // param.planId = '*';
      param.data = [];
      param.match = '';
      param.nearMe = '';

      vm.isReset = true;
      vm.groupProviders = [];
      vm.hasProviders = false;
      vm.selectedCity = '';
      // vm.selectedPlan = '*';

      provider.setSelected(provider.keys.selectedCity, vm.selectedCity);
      // provider.setSelected(provider.keys.selectedPlan, vm.selectedPlan);

      angular.copy({ providerCity: '', match: '' }, filter);

      $timeout(function o() {
        vm.isReset = false;
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    function changeCity(event) {
      if (event && event.target) {
        $timeout(function o() {
          vm.selectedCity = event.target.value || '';
        });
      }
    }

    function changePlan(event) {
      if (event && event.target) {
        $timeout(function o() {
          vm.selectedPlan = event.target.value || '';
        });
      }
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#search
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Search providers of the member by name.
     *
     * @param {string} match Name of the provider.
     */
    function search(match) {
      param.data = [];
      param.skip = 0;
      param.match = match;

      vm.groupProviders = [];
      vm.hasProviders = false;

      $timeout(function o() {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#submit
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get providers of the member with filtering.
     *
     * @param {Object} data Object describing the filtering data.
     *  The object has following properties:
     *  - **providerCity** - `{string}` - City of the provider.
     *  - **planId** - `{string}` - ID of the plan.
     *  - **match** - `{string}` - Keyword of the provider.
     */
    function submit(data) {
      if (data && data.planId === '*') {
        alertBenefit();
        return;
      }
      if (data) {
        if (angular.isDefined(data.providerCity)) vm.selectedCity = data.providerCity;
        if (angular.isDefined(data.planId)) vm.selectedPlan = data.planId;
      }

      param.data = [];
      param.skip = 0;

      vm.groupProviders = [];
      vm.hasProviders = false;

      provider.setSelected(provider.keys.selectedCity, vm.selectedCity);
      provider.setSelected(provider.keys.selectedPlan, vm.selectedPlan);

      vm.hasSelectedCityAndPlan = true;

      angular.merge(param, data, { providerCity: vm.selectedCity, planId: vm.selectedPlan });

      $timeout(function o() {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#sort
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Get providers of the member with sorting by name.
     */
    function sort() {
      param.sort = _.first(param.sort) === '-' ? 'providerName' : '-providerName';
      param.skip = 0;
      param.data = [];

      vm.groupProviders = [];
      vm.hasProviders = false;

      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    function parsePhone(phone) {
      var n = 0;
      return phone.replace(/\(|\)/g, '').replace(/-/g, function o(m) {
        n += 1;
        return n > 1 ? '/' : m;
      }).replace(/,|;|\\|\|/g, '/').replace(/\/.*$/g, '');
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#show
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Display details of provider based on selected item.
     *
     * @param {Object} item Object describing the provider details.
     */
    function show(item) {
      var scope = $scope;

      scope.data = item;
      scope.data.call = call;
      scope.data.openMaps = openMaps;
      scope.data.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js';
      scope.data.parsePhone = parsePhone;

      if (maps && maps.apiKey) {
        scope.data.googleMapsUrl = scope.data.googleMapsUrl.concat('?key=' + maps.apiKey);
      }

      $ionicPopup.alert({
        scope: scope,
        templateUrl: 'app/main/admedika/provider/modal/show.html',
        cssClass: 'popup-primary provider-popup',
        okText: 'Tutup',
        okType: 'button-default'
      });
    }

    /**
     * @ngdoc function
     * @name app.main.admedika.provider.controller#openMaps
     * @methodOf app.main.admedika.provider.controller:MainAdmedikaProviderController
     *
     * @description
     * Invoke maps apps with pin based on selected location.
     *
     * @param {string} lat Latitute of the location.
     * @param {string} lng longitude of the location.
     */
    function openMaps(lat, lng) {
      var position = [lat, lng].join(',');
      var location = $window.location;
      var Platform = $window.ionic && $window.ionic.Platform;
      var href = 'http://maps.google.com/maps?&z=15&q=' + lat + '+' + lng;

      if (Platform.isAndroid()) {
        href = 'geo://?q='.concat(position);
      } else if (Platform.isIOS() || Platform.isIPad()) {
        href = 'maps://maps.apple.com/?q='.concat(position);
      }

      location.href = href;
    }
  }

  angular.module('app.main.admedika.provider').controller('MainAdmedikaProviderController', MainAdmedikaProviderController);
}());
