(function _AppMainController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.main.controller:MainController
   *
   * @requires $q
   * @requires $scope
   * @requires $state
   * @requires $rootScope
   * @requires $cordovaInAppBrowser
   * @requires app.providers.graphql.service:graphql
   * @requires EVENT
   *
   * @description
   * Main controller
   */
  function MainController($q, $scope, $rootScope, $cordovaInAppBrowser, graphql, EVENT) {
    var $this = $rootScope;
    var vm = this;

    vm.isLoading = false;
    vm.profile = {};
    vm.openUrl = openUrl;
    vm.menus = [];

    getProfile();
    // get url from menu cms index=2
    getUrl();

    $this.$on(EVENT.PHOTO.CHANGED, function changed(event, args) {
      $scope.$evalAsync(function sync() {
        angular.merge(vm.profile, { photo: args });
      });
    });

    function cardNotFound(err) {
      var query = [
        'query Profile {',
        '  profile {',
        '    admedika {',
        '      cardNumber',
        '    }',
        '  }',
        '}'
      ].join('');

      graphql(query)
        .then(function resolve(res) {
          var data = {
            profile: { admedika: { member: { responseDescription: err } } }
          };
          angular.merge(data, angular.copy(res));
          angular.copy(data.profile, vm.profile);
        });
    }

    /**
     * @ngdoc function
     * @name app.main.controller#getProfile
     * @methodOf app.main.controller:MainController
     *
     * @description
     * Get user profile.
     */
    function getProfile() {
      var query = [
        'query Profile {',
        '  profile {',
        '    name',
        '    photo',
        '    admedika {',
        '      cardNumber',
        '      member {',
        '        fullName',
        '        dateOfBirth',
        '        memberType',
        '        activeFlag',
        '        responseCode',
        '        responseDescription',
        '      }',
        '      plans {',
        '        planId',
        '        policyStartDate',
        '        policyEndDate',
        '      }',
        '    }',
        '  }',
        '}'
      ].join('');

      $q.resolve(true)
        .then(function resolve() {
          vm.isLoading = true;
          return graphql(query);
        })
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.profile;
        })
        .then(function resolve(res) {
          if (res) angular.copy(res, vm.profile);
          // else $this.$root.logout();
        })
        .catch(function reject(err) {
          if (/not found/i.test(err)) cardNotFound(err);
        })
        .finally(function done() {
          vm.isLoading = false;
        });
    }

    function getUrl() {
      var query = [
        'query Sliders($match: String, $limit: Int, $sort: String) {',
        '  sliders(match: $match, limit: $limit, sort: $sort) {',
        '    _id',
        '    index',
        '    uri',
        '    isExternal',
        '  }',
        '}'
      ].join('');
      var variable = {
        sort: 'index',
        limit: Infinity,
        match: angular.toJson({ page: { $regex: 'menu', $options: 'i' } })
      };

      graphql(query, variable)
        .then(function resolve(res) {
          return angular.copy(res);
        })
        .then(function resolve(res) {
          vm.menus = res.sliders;
        });
    }

    function openUrl(index) {
      // index on menu slider
      // 1=buku polis
      // 2=polis medicare
      var url = vm.menus[index].uri;
      try {
        $cordovaInAppBrowser.open(url, '_system', { zoom: 'no' });
      } catch (e) {
        location.href = url;
      }
    }
  }

  angular.module('app.main').controller('MainController', MainController);
}());
