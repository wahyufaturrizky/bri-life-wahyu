(function _AppConstant_() {
  'use strict';

  var ANALYTIC = {
    ENABLE: false
  };

  var EVENT = {
    LOGIN: {
      SUCCESS: 'EventLoginSuccess'
    },
    LOGOUT: {
      SUCCESS: 'EventLogoutSuccess'
    },
    PHOTO: {
      CHANGED: 'EventPhotoChanged'
    }
  };

  var GRAPHQL = {
    // URL: 'https://api.mypersona.id/graphql'
    // URL: 'http://139.59.98.26:4321/graphql'
    // URL: 'http://128.199.163.154:5321/graphql'
    URL: 'https://api.brilife.mypersona.id/graphql'
    // URL: 'http://localhost/graphql'
    // URL: 'http://192.168.0.183:4321/graphql'
    // URL: 'http://192.168.43.152:4321/graphql'
  };

  var IMAGE = {
    AVATAR: 'img/default.png'
  };

  angular
    .module('app')
      .constant('ANALYTIC', ANALYTIC)
      .constant('EVENT', EVENT)
      .constant('GRAPHQL', GRAPHQL)
      .constant('IMAGE', IMAGE);
}());
