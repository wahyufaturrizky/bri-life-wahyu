(function _AppAuthActivateController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.auth.activate.controller:AuthActivateController
   *
   * @requires $ionicPopup
   * @requires $state
   * @requires $q
   * @requires app.providers.graphql.service:graphql
   * @requires app.providers.toast.factory:toast
   *
   * @description
   * Activate account controller.
   */
  function AuthActivateController($ionicPopup, $state, $stateParams, $q, graphql, toast) {
    var vm = this;

    vm.email = $stateParams.email;
    vm.isLoading = false;
    vm.isSending = false;
    vm.resend = resend;
    vm.submit = submit;

    /**
     * @ngdoc method
     * @name app.auth.activate.controller#resend
     * @methodOf app.auth.activate.controller:AuthActivateController
     *
     * @description
     * Resend code activation.
     *  If success, a success popup will be shown.
     *  Otherwise, an error popup will be shown.
     *
     * @param {Object} variables Object describing the user credentials.
     *  The object has following properties:
     *  - **email** - `{string}` - Email of the user.
     */
    function resend(email) {
      var query = [
        'mutation ResendCode($email: String!) {',
        '  resendCode(email: $email)',
        '}'
      ].join('');

      $q.resolve(true)
        .then(function resolve() {
          vm.isSending = true;
          return graphql(query, { email: email });
        })
        .then(function resolve(res) {
          var data = angular.copy(res);
          return data && data.resendCode;
        })
        .then(function resolve(res) {
          if (!res) throw new Error();
          return $ionicPopup.alert({
            templateUrl: 'app/auth/activate/modal/resent.html',
            cssClass: 'reconnect-popup',
            okText: 'Tutup',
            okType: 'button-default'
          });
        })
        .catch(function reject(err) {
          toast.shortBottom(err.toString());
        })
        .finally(function end() {
          vm.isSending = false;
        });
    }

    /**
     * @ngdoc method
     * @name app.auth.activate.controller#submit
     * @methodOf app.auth.activate.controller:AuthActivateController
     *
     * @description
     * Processing activate account.
     *  If success, user will be redirected to login page.
     *  Otherwise, an error popup will be shown.
     *
     * @param {Object} variables Object describing the user credentials.
     *  The object has following properties:
     *  - **code** - `{string}` - Activation code of the user.
     */
    function submit(variables) {
      var query = [
        'mutation Activate($code: String!) {',
        '  activate(code: $code) {',
        '    auth {',
        '      isActive',
        '    }',
        '  }',
        '}'
      ].join('');

      $q.resolve(true)
        .then(function resolve() {
          vm.isLoading = true;
          return graphql(query, variables);
        })
        .then(function resolve(res) {
          var data = angular.copy(res);
          return data && data.activate && data.activate.auth && data.activate.auth.isActive;
        })
        .then(function resolve(res) {
          if (!(res)) throw new Error();
          return $ionicPopup.alert({
            templateUrl: 'app/auth/activate/modal/success.html',
            cssClass: 'reconnect-popup',
            okText: 'Log in',
            okType: 'button-default'
          });
        })
        .then(function resolve() {
          $state.go('app.auth.login');
        })
        .catch(function reject() {
          $ionicPopup.alert({
            templateUrl: 'app/auth/activate/modal/failed.html',
            cssClass: 'reconnect-popup',
            okText: 'Tutup',
            okType: 'button-default'
          });
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }
  }

  angular.module('app.auth.activate').controller('AuthActivateController', AuthActivateController);
}());
