(function _AppAuthSupportController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.auth.support.controller:AuthSupportController
   *
   * @requires $ionicHistory
   * @requires $q
   * @requires analytic
   * @requires app.providers.graphql.service:graphql
   * @requires app.providers.toast.factory:toast
   *
   * @description
   * Contact us controller.
   */
  function AuthSupportController(
    $ionicHistory, $ionicPopup, $q, $stateParams, analytic, graphql, toast
  ) {
    var vm = this;

    vm.email = $stateParams.email;
    vm.isLoading = false;
    vm.submit = submit;

    /**
     * @ngdoc function
     * @name app.auth.support.controller#submit
     * @methodOf app.auth.support.controller:AuthSupportController
     *
     * @description
     * Processing send inquiry.
     *  If success, a success toast will be shown.
     *  Otherwise, an error toast will be shown.
     *
     * @param {Object} variables Object describing the inquiry.
     *  The object has following properties:
     *  - **subject** - `{string}` - Subject of the inquiry.
     *  - **content** - `{string}` - Content of the inquiry.
     */
    function submit(variables) {
      var query = [
        'mutation AddInquiry($email: String, $subject: String, $content: String!) {',
        '  addInquiry(email: $email, subject: $subject, content: $content) {',
        '    _id',
        '  }',
        '}'
      ].join('');

      $q.resolve(true)
        .then(function resolve() {
          vm.isLoading = true;
          return graphql(query, variables);
        })
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.addInquiry && result.addInquiry._id;
        })
        .then(function resolve(res) {
          if (!(res)) throw new Error();
          analytic.addInquiry();
          return $ionicPopup.alert({
            templateUrl: 'app/auth/support/modal/success.html',
            cssClass: 'mp-popup-style',
            okText: 'Close'
          });
        })
        .then(function resolve() {
          $ionicHistory.goBack();
        })
        .catch(function reject(err) {
          toast.shortBottom(err.toString());
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }
  }

  angular.module('app.auth.support').controller('AuthSupportController', AuthSupportController);
}());
