(function _AppAuthSupportRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.auth.support', {
      url: '/support?email',
      views: {
        'menuContent@app.auth': {
          templateUrl: 'app/auth/support/index.html',
          controller: 'AuthSupportController',
          controllerAs: '$'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/auth/support/controller.js');
        }
      }
    });
  }

  angular.module('app.auth.support').config(route);
}());
