(function _AppAuthSupportModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.auth.support
   *
   * @description
   * Contact us module.
   *
   * ### Route
   * `/auth/support`
   *
   * ### Controller
   * - {@link app.auth.support.controller:AuthSupportController}
   */
  angular.module('app.auth.support', modules);
}());
