(function _AppAuthLoginController_() {
  'use strict';

  /**
   * @ngdoc controller
   * @name app.auth.login.controller:AuthLoginController
   *
   * @requires $ionicHistory
   * @requires $ionicPopup
   * @requires $log
   * @requires $rootScope
   * @requires $scope
   * @requires $state
   * @requires $timeout
   * @requires $window
   * @requires EVENT
   * @requires app.providers.auth.service:auth
   * @requires app.providers.graphql.service:graphql
   *
   * @description
   * Login controller.
   */
  function AuthLoginController(
    $ionicHistory, $ionicPopup, $log, $rootScope, $scope, $state, $timeout, $window,
    EVENT, auth, graphql, $ionicSlideBoxDelegate, $cordovaInAppBrowser
  ) {
    var vm = this;
    var location = $window.location;
    var widthPage = $window.innerWidth;
    var heightPage = $window.innerHeight;

    vm.interval = 4321;
    vm.widthPage = widthPage;
    vm.heightPage = heightPage;
    vm.isLoading = false;
    vm.submit = submit;
    vm.images = [];
    vm.slideChanged = slideChanged;
    vm.open = open;

    $timeout(calcHeight, 1000);
    $timeout(getImages);

    function open(img) {
      if (img && img.uri) {
        if (img.isExternal) {
          try {
            $cordovaInAppBrowser.open(img.uri, '_system', { zoom: 'no' });
          } catch (e) {
            location.href = img.uri;
          }
        } else $state.go(img.uri);
      }
    }

    function calcHeight() {
      var element2 = angular.element($window.document.querySelector('#formLogin'));
      var heightImage = $window.innerHeight - element2[0].clientHeight;

      vm.heightImage = heightImage + 'px';
    }

    function slideChanged(index) {
      if (($ionicSlideBoxDelegate.slidesCount() - 1) === index) {
        $timeout(function o() {
          $ionicSlideBoxDelegate.slide(0);
        }, vm.interval);
      }
    }

    function getImages() {
      var query = [
        'query Sliders($match: String, $limit: Int, $sort: String) {',
        '  sliders(match: $match, limit: $limit, sort: $sort) {',
        '    _id',
        '    index',
        '    image',
        '    uri',
        '    isExternal',
        '  }',
        '}'
      ].join('');
      var variable = {
        sort: 'index',
        limit: Infinity,
        match: angular.toJson({ page: { $regex: 'login', $options: 'i' } })
      };

      graphql(query, variable)
        .then(function resolve(res) {
          return angular.copy(res);
        })
        .then(function resolve(res) {
          vm.images = res.sliders;
          $timeout(function o() {
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.start();
            $scope.$apply();
          });
        });
    }

    /**
     * @ngdoc function
     * @name app.auth.login.controller#submit
     * @methodOf app.auth.login.controller:AuthLoginController
     *
     * @description
     * Processing login authentication.
     *  If success, user will be redirected to home page.
     *  Otherwise, an error popup will be shown.
     *
     * @param {Object} variables Object describing the user credentials.
     *  The object has following properties:
     *  - **username** - `{string}` - Username or email of the user.
     *  - **password** - `{string}` - Password of the user.
     */
    function submit(variables) {
      var query = [
        'mutation Login($username: String!, $password: String!) {',
        '  login(username: $username, password: $password) {',
        '    token',
        '  }',
        '}'
      ].join('');

      vm.isLoading = true;

      if (!(variables && variables.username && variables.password)) {
        $ionicPopup.alert({
          templateUrl: 'app/auth/login/modal/no-credential.html',
          cssClass: 'mp-popup-style',
          okText: 'Tutup'
        });

        vm.isLoading = false;

        return;
      }

      graphql(query, variables)
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.login && result.login.token;
        })
        .then(function resolve(res) {
          if (!(res)) throw new Error('No token available');
          return auth.setToken(res);
        })
        .then(function resolve() {
          return $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true,
            historyRoot: true
          });
        })
        .then(function resolve() {
          $rootScope.$emit(EVENT.LOGIN.SUCCESS);
          $state.go('app.main.home');
        })
        .catch(function reject(err) {
          var scope = $scope;

          if (/belum diaktivasi|not activated/i.test(err)) {
            $state.go('app.auth.activate', { email: variables.username });
            return;
          }

          scope.data = err;

          $log.debug(err);
          $ionicPopup.alert({
            scope: scope,
            templateUrl: 'app/auth/login/modal/login-error.html',
            cssClass: 'mp-popup-style',
            okText: 'Tutup'
          });
        })
        .finally(function end() {
          vm.isLoading = false;
        });
    }
  }

  angular.module('app.auth.login').controller('AuthLoginController', AuthLoginController);
}());
