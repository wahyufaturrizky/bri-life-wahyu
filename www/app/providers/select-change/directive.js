(function _AppProvidersSelectChangeDirective_() {
  'use strict';

  /**
   * @ngdoc directive
   * @name app.providers.select-change.directive:select-change
   * @restrict A
   *
   * @description
   * Select change.
   */
  function selectChange() {
    function link($scope, $element, $attrs) {
      var onChangeHandler = $scope.$eval($attrs.selectChange);
      $element.bind('change', onChangeHandler);
    }
    return {
      restict: 'A',
      link: link
    };
  }
  angular.module('app.providers.select-change').directive('selectChange', selectChange);
}());
