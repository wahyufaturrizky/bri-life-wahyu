(function _AppProvidersSelectChangeModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.providers.select-change
   *
   * @description
   * select-change module.
   *
   * ### Directive
   * - {@link app.providers.select-change.directive:select-change}
   */
  angular.module('app.providers.select-change', modules);
}());
