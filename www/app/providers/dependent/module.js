(function _AppProvidersDependentModule_() {
  'use strict';

  var modules = [
    //
  ];

  /**
   * @ngdoc overview
   * @name app.providers.dependent
   *
   * @description
   * Dependent module.
   *
   * ### Dependencies
   *
   *
   * ### Provider
   * - {@link app.providers.dependent.service:dependent}
   */
  angular.module('app.providers.dependent', modules);
}());
