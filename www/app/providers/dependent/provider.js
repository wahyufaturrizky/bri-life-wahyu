(function _AppProviderDependentProvider_() {
  'use strict';

  /** @this */
  function dependent() {
    var config = { key: 'dependent' };

    /**
     * @ngdoc property
     * @name app.providers.dependent.service#set
     * @propertyOf app.providers.dependent.service:dependent
     *
     * @description
     * Object containing default values for dependent configurations.
     * The object has following properties:
     *  - **key** - `{string}` - Key for saving token on storage. Default value is `dependent`.
     *
     * @example
       <example module="app">
         <file name="index.html"></file>
         <file name="script.js">
           angular.module('app', ['app.providers.dependent'])
            .config(function (dependentProvider) {
              dependentProvider.set({
                key: 'dependent'
              });
            });
         </file>
       </example>
     */
    this.set = function set(userConfig) {
      angular.merge(config, userConfig);
    };

    /**
     * @ngdoc service
     * @name app.providers.dependent.service:dependent
     *
     * @requires $window
     *
     * @description
     * Dependent service.
     */
    this.$get = function $get($window) {
      /**
       * @ngdoc method
       * @name app.providers.dependent.service#remove
       * @methodOf app.providers.dependent.service:dependent
       *
       * @description
       * Remove dependent on session storage.
       */
      function remove() {
        $window.sessionStorage.removeItem(config.key);
      }

      /**
       * @ngdoc method
       * @name app.providers.dependent.service#get
       * @methodOf app.providers.dependent.service:dependent
       *
       * @description
       * Get current dependent
       *
       * @returns {Object} Object describing the dependent.
       */
      function get() {
        var payload = {};
        try {
          payload = angular.fromJson($window.sessionStorage.getItem(config.key));
        } catch (e) {
          //
        }
        return payload;
      }

      /**
       * @ngdoc method
       * @name app.providers.dependent.service#set
       * @methodOf app.providers.dependent.service:dependent
       *
       * @description
       * Save dependent on session storage.
       *
       * @param {Object} data Dependent to be saved.
       */
      function set(data) {
        return $window.sessionStorage.setItem(config.key, angular.toJson(data));
      }

      return {
        remove: remove,
        get: get,
        set: set
      };
    };
  }

  angular.module('app.providers.dependent').provider('dependent', dependent);
}());
