(function _AppProvidersHelpLineModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.providers.help-line
   *
   * @description
   * Phone call module.
   *
   * ### Directive
   * - {@link app.providers.help-line.directive:help-line}
   */
  angular.module('app.providers.help-line', modules);
}());
