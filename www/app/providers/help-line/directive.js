(function _AppProvidersHelpLineDirective_() {
  'use strict';

  /**
   * @ngdoc directive
   * @name app.providers.help-line.directive:help-line
   * @restrict A
   *
   * @description
   * Call phone number.
   */
  function helpLine($window, graphql, toast) {
    var apps = {};
    var query = [
      'query AppsConfig {',
      '  config {',
      '    apps {',
      '      helpLine',
      '    }',
      '  }',
      '}'
    ].join('');

    graphql(query)
      .then(function resolve(res) {
        var result = angular.copy(res);
        return result && result.config && result.config.apps;
      })
      .then(function resolve(res) {
        angular.copy(res, apps);
      })
      .catch(function reject() {
        //
      });

    function link(scope, element, attrs) {
      element.on('click', function click(event) {
        var CallNumber = $window.plugins && $window.plugins.CallNumber;

        event.preventDefault();

        if (CallNumber && CallNumber.callNumber && (attrs.helpLine || apps.helpLine)) {
          CallNumber.callNumber(_.noop, onError, attrs.helpLine || apps.helpLine, true);
        } else {
          onError();
        }
      });

      function onError(error) {
        toast.shortBottom(error || 'Tidak bisa melakukan panggilan telepon');
      }
    }

    return {
      restict: 'A',
      link: link
    };
  }

  angular.module('app.providers.help-line').directive('helpLine', helpLine);
}());
