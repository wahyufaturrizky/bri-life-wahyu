(function _AppProvidersOneSignalModule_() {
  'use strict';

  var modules = [];

  /**
   * @ngdoc overview
   * @name app.providers.onesignal
   *
   * @description
   * Toast module.
   *
   * ### Factory
   * - {@link app.providers.onesignal.factory:onesignal}
   */
  angular.module('app.providers.onesignal', modules);
}());
