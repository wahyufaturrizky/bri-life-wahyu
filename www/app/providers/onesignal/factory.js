(function _AppProviderOneSignalFactory_() {
  'use strict';

  /**
   * @ngdoc service
   * @name app.providers.onesignal.factory:onesignal
   *
   *
   * @description
   * OneSigna factory.
   */
  function onesignal($q, graphql) {
    var config = {};

    function $config() {
      var query = [
        'query PushConfig {',
        '  config {',
        '    push {',
        '      appId',
        '    }',
        '  }',
        '}'
      ].join('');

      if (config && config.appId) return $q.resolve(config);
      return graphql(query)
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.config && result.config.push;
        })
        .then(function resolve(res) {
          angular.merge(config, res);
          return config;
        })
        .catch(function reject() {
          //
        });
    }

    function init() {
      document.addEventListener('deviceready', function c() { // eslint-disable-line
        $config().then(function resolve(res) {
          if (!(res && res.appId)) throw new Error('Push config unavailable.');
          window.plugins.OneSignal // eslint-disable-line
            .startInit(res.appId)
            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification) // eslint-disable-line
            .endInit();
        });
      }, false);
    }

    function sendTags(tags) {
      document.addEventListener('deviceready', function c() { // eslint-disable-line
        window.plugins.OneSignal // eslint-disable-line
          .sendTags(tags);
      }, false);
    }

    function setSubscription(subscription) {
      document.addEventListener('deviceready', function c() { // eslint-disable-line
        window.plugins.OneSignal // eslint-disable-line
          .setSubscription(subscription);
      }, false);
    }

    function deleteTags(tags) {
      document.addEventListener('deviceready', function c() { // eslint-disable-line
        window.plugins.OneSignal // eslint-disable-line
          .deleteTags(tags);
      }, false);
    }

    return {
      init: init,
      sendTags: sendTags,
      setSubscription: setSubscription,
      deleteTags: deleteTags
    };
  }

  angular.module('app.providers.onesignal').factory('onesignal', onesignal);
}());
