(function _AppProvidersMyAdMedikaFactory_() {
  'use strict';

  /**
   * @ngdoc service
   * @name app.providers.myadmedika.factory:myadmedika
   *
   * @requires $q
   * @requires $state
   * @requires $timeout
   * @requires $window
   * @requires app.providers.graphql
   * @requires app.providers.toast
   *
   * @description
   * MyAdMedika factory.
   */
  function myadmedika($q, $state, $timeout, $window, graphql, toast) {
    var config;
    var location = $window.location;
    var Platform = $window.ionic && $window.ionic.Platform;
    var query = [
      'query AppsConfig {',
      '  config {',
      '    apps {',
      '      android',
      '      ios',
      '      browser',
      '    }',
      '  }',
      '}'
    ].join('');

    graphql(query).then(function resolve(res) {
      var result = angular.copy(res);
      return result && result.config && result.config.apps;
    }).then(function resolve(res) {
      config = angular.copy(res);
    });

    function check() {
      $q.when(config)
        .then(function resolve(res) {
          if (!(res)) throw new Error('Please wait...');
          return (Platform.isIOS() || Platform.isIPad());
        })
        .then(function resolve(res) {
          if (res) return config.ios;
          if (Platform.isAndroid()) return config.android;
          return config.browser;
        })
        .then(function resolve(res) {
          location.href = res;
        })
        .catch(function reject(err) {
          toast.shortBottom(err.toString().replace('Error:', ''));
          $timeout(check, 2345);
        });
    }

    function open(params) {
      var admedika;
      try {
        admedika = angular.fromJson(params || {});
      } catch (e) {
        admedika = {};
      }
      if (admedika && admedika.cardNumber) {
        if (isActive(admedika.member) &&
           !isExpired(admedika.plans) &&
           !isLost(admedika.member)) {
          $state.go('app.main.admedika.member');
        } else $state.go('app.main.admedika.reconnect', { cardNumber: admedika.cardNumber });
      } else $state.go('app.main.admedika.reconnect');
    }

    function isLost(member) {
      if (member && /not found/i.test(member.responseDescription)) return true;
      return false;
    }

    function isActive(member) {
      if (member && member.activeFlag) {
        return member.activeFlag === 'y' || member.activeFlag === 'Y' || /^active$/i.test(member.activeFlag);
      }
      return false;
    }

    function isExpired(plans) {
      return isExpiredInAdcps(plans);
    }

    function isExpiredInAdcps(plans) {
      var expired = false;
      if (plans && plans.length) {
        expired = !_.some(plans, function c(plan) {
          var end = plan.policyEndDate;
          var now = moment().format('MMM-DD-YYYY');
          return moment(end, 'MMM-DD-YYYY').isValid() && moment(end, 'MMM-DD-YYYY').isSameOrAfter(now, 'day');
        });
      }
      return expired;
    }

    return {
      check: check,
      isActive: isActive,
      isExpired: isExpired,
      isLost: isLost,
      open: open
    };
  }

  angular
    .module('app.providers.myadmedika')
      .factory('myadmedika', myadmedika);
}());
