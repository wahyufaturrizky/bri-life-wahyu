(function _AppProvidersSliderFactory_() {
  'use strict';

  /**
   * @ngdoc service
   * @name app.providers.slider.factory:slider
   *
   * @requires $base64
   * @requires app.providers.graphql
   *
   * @description
   * Slider factory.
   */
  function slider($base64, $http, $q, graphql) {
    var config = {};

    function $config() {
      var query = [
        'query CdnConfig {',
        '  config {',
        '    cdn {',
        '      host',
        '      username',
        '      password',
        '    }',
        '  }',
        '}'
      ].join('');

      if (config && config.host) return $q.resolve(config);
      return graphql(query)
        .then(function resolve(res) {
          var result = angular.copy(res);
          return result && result.config && result.config.cdn;
        })
        .then(function resolve(res) {
          angular.merge(config, res);
          return config;
        })
        .catch(function reject() {
          //
        });
    }

    /**
     * @ngdoc method
     * @name app.providers.slider.factory#upload
     * @methodOf app.providers.slider.factory:slider
     *
     * @description
     * Upload file to slider server.
     *
     * @param {file} file File to be uploaded.
     */
    function upload(data) {
      return $config().then(function resolve(res) {
        if (!res) throw new Error('CDN config unavailable.');
        return $http({
          method: 'POST',
          url: [res.host, 'slider'].join('/'),
          headers: {
            Authorization: 'Basic ' + $base64.encode(res.username + ':' + res.password),
            'Content-Type': undefined
          },
          transformRequest: angular.identity,
          data: data
        });
      });
    }

    return {
      config: $config,
      upload: upload
    };
  }

  angular.module('app.providers.slider').factory('slider', slider);
}());
