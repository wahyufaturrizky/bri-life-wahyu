(function _AppProvidersSliderFilter_() {
  'use strict';

  /**
   * @ngdoc filter
   * @name app.providers.slider.filter:slider
   *
   * @requires app.providers.slider
   *
   * @description
   * Return full url of file.
   *
   * @example
     <example module="app">
       <file name="index.html">
         <img ng-src="{{ scope.profile.photo | slider }}" />
       </file>
     </example>
   */
  function filter(slider) {
    var isWaiting = false;
    var host = null;

    get.$stateful = true;

    function get(file) {
      var value = '';
      if (!file) return undefined;
      if (host) value = [host, 'slider', file].join('/');
      else {
        if (isWaiting === false) {
          isWaiting = true;
          slider.config().then(function resolve(res) {
            host = res.host;
            isWaiting = false;
          });
        }
        return value;
      }
      return value;
    }

    return get;
  }

  angular.module('app.providers.slider').filter('slider', filter);
}());
