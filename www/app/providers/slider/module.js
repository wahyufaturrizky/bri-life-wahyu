(function _AppProvidersSliderModule_() {
  'use strict';

  var modules = [
    'base64'
  ];

  /**
   * @ngdoc overview
   * @name app.providers.slider
   *
   * @description
   * Slider module.
   *
   * ### Dependencies
   * - {@link https://github.com/ninjatronic/angular-base64 base64}
   *
   * ### Factory
   * - {@link app.providers.slider.factory:slider}
   *
   * ### Filter
   * - {@link app.providers.slider.filter:slider}
   */
  angular.module('app.providers.slider', modules);
}());
