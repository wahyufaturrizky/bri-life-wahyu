# MyPersona Mobile
[![build status](http://git.lussa.net/admedika/mypersona-mobile/badges/master/build.svg)](http://git.lussa.net/admedika/mypersona-mobile/commits/master)

### Requirements

```
Node.js >= 4.0
Bower   >= 1.8
Gulp    >= 3.9
Ionic   >= 1.7
```

Update March 2020

```
Node.js 10.13.0
NPM 6.4.1
Bower 1.8.8
Gulp 3.9.1
Ionic CLI 6.2.0
Cordova 9.0.0
```

You should build the ios platform using Ionic CLI >= 6 to ensure the WKWebView usage.

### Installation

if you're using IonicCLI 1.x.x
```sh
$ git clone http://git.lussa.net/admedika/mypersona-mobile.git mypersona-mobile
$ cd mypersona-mobile
$ npm install
$ bower install
$ ionic resources
$ ionic platform add [android|ios]
$ ionic build [android|ios]
```

If your using IonicCLI 3.x.x
```sh
$ git clone http://git.lussa.net/admedika/mypersona-mobile.git mypersona-mobile
$ cd mypersona-mobile
$ npm install
$ bower install
$ gulp compile
$ ionic cordova platform add [android|ios]
$ ionic cordova resources
$ ionic cordova build [android|ios]
```

### Development
MyPersona Mobile uses [Gulp](http://gulpjs.com) for fast developing.
Make a change in your file and instantanously see your updates!
MyPersona Mobile also use [ESLint](http://eslint.org) as a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code.
Make sure to obey the rules!

Open your favorite Terminal and run these commands.
```sh
$ ionic serve
```

### Test
Start `appium` and run these commands.
```sh
$ npm test
```

### Publishing

The app name on app/play store and on the phone should be different.

- On the store : `MyPersona - by AdMedika`  (notice the capital M on AdMedika).
- On the app : `MyPersona`

To accomodate this, don't forget to change the display name manually on each platform (changing via config.xml in cordova doesn't work) before deploying / publishing / re-adding platform.

- On iOS : open xcode project, on the `Display Name`, type `MyPersona`
- On android : open `platforms/android/res/values/string.xml`, change `<string name="launcher_name">@string/app_name</string>` to `<string name="launcher_name">MyPersona</string>`

On iOS publishing, don't forget to add app store icon (1024x1024)  on xcode - resources if you re-adding the platform. The icon is on google drive -> DEV -> design.

### Troubleshooting

If you're using the latest IonicCLI (3.x.x), for some reasons the gulp process might not be executed well along with the Ionic command. Hence, try using one of this command if you experience **blank screen, stuck on splashscreen, or screwed UI.**

```bash
# on development
gulp compile

# on deployment for production IPA/APK
gulp compile:prod
```

Before building IPA / APK for client testing or publishing, ensure you already run `gulp compile:prod`

### License
MIT
